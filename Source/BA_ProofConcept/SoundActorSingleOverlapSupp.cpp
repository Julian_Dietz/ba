#include "BA_ProofConcept.h"
#include "SoundActorSingleOverlapSupp.h"

#include "BAMusicalLibrary.h"


ASoundActorSingleOverlapSupp::ASoundActorSingleOverlapSupp():
	bFadingIn(false)
{
}

void ASoundActorSingleOverlapSupp::BeginPlay()
{
	Super::BeginPlay();

	ensure(CubeMaterial);
	if (CubeMaterial)
	{
		UStaticMeshComponent* Mesh = FindComponentByClass<UStaticMeshComponent>();
		Mesh->CreateDynamicMaterialInstance(0, CubeMaterial);
	}
}

void ASoundActorSingleOverlapSupp::Tick(float DeltaTime) 
{
	Super::Tick(DeltaTime);
	// do not show this code to anyone, ever
	if (bFadingIn)
	{
		if (UStaticMeshComponent* Mesh = FindComponentByClass<UStaticMeshComponent>())
		{
			BAMusicalLibrary::FadeInMesh(Mesh, DeltaTime);
			bFadingIn = Mesh != nullptr;
			if (!bFadingIn)
			{
				StartLayerChallenge();
			}
		}
	}
}

void ASoundActorSingleOverlapSupp::StartFadingIn()
{
	bFadingIn = true;
}

void ASoundActorSingleOverlapSupp::OnBeginOverlap(AActor* OverlappedActor, AActor* OtherActor)
{
	if (NoteStateOnInput == ENoteState::Invalid)
	{
		NoteStateOnInput = CurrentNoteState;
		OnInput(NoteStateOnInput);
	}
}

void ASoundActorSingleOverlapSupp::OnNoteStateChanged(EInstrumentType InInstrumentType, int32 NoteThatChangedIndex, ENoteState NewNoteState)
{
	if (InInstrumentType == InstrumentType)
	{
		//PrintEnumDebug(TEXT("ENoteState"), (uint8)CurrentNoteState, 1);

		if (NewNoteState ==  ENoteState::OnBeat)
		{
			CurrentNoteState = NewNoteState;
		}
		else
		{
			NoteStateOnInput = ENoteState::Invalid;
		}
	}
}

