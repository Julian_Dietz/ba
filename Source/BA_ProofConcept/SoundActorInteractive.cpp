#include "BA_ProofConcept.h"
#include "SoundActorInteractive.h"

#include "BAFulfillComponent.h"
#include "NoteManager.h"

ASoundActorInteractive::ASoundActorInteractive() :
	bWaitUntilLoopForFeedback(false),
	bAutoAdjustUnlockSelfComp(true),
	bPrintDebugInfo(false),
	NumCorrectHitsNeeded(8),
	NumCorrectLoopsNeeded(1),
	CubeMaterial(nullptr),
	FulfillCompSelf(CreateDefaultSubobject<UBAFulfillComponent>(TEXT("FulfillCompSelf"))),
	NumCorrectHits(0),
	NumCorrectLoops(0),
	bHadAllInputs(false),
	bStartedChallenge(false),
	bFinished(false)
{
}

void ASoundActorInteractive::BeginPlay()
{
	Super::BeginPlay();
	
	if (bAutoAdjustUnlockSelfComp)
	{
		FulfillCompSelf->SetInstrumentType(InstrumentType);
	}

	for (TActorIterator<ANoteManager> ItNoteManager(GetWorld()); ItNoteManager; ++ItNoteManager)
	{
		ItNoteManager->OnLoop.AddDynamic(this, &ASoundActorInteractive::OnLoop);
	}
}

void ASoundActorInteractive::OnNoteStateChanged(EInstrumentType InInstrumentType, int32 NoteThatChangedIndex, ENoteState NewNoteState)
{
	// Only here because I had trouble making this class abstract in Unreal.
}

void ASoundActorInteractive::OnBeat(EInstrumentType InInstrumentType, const FMusicalNote& InNextNote)
{
	if (!bFinished && bStartedChallenge)
	{
		Super::OnBeat(InInstrumentType, InNextNote);
	}
}

void ASoundActorInteractive::OnLoop()
{
	if (!bHadAllInputs)
	{
		if (GetNumCorrectHits() >= NumCorrectHitsNeeded)
		{
			++NumCorrectLoops;
			if (NumCorrectLoops >= NumCorrectLoopsNeeded)
			{
				bHadAllInputs = true;
				OnGaveEnoughInputsOnSoundActor.Broadcast(this);
				bFinished = true;
			}
		}

		Reset();
	}
}

void ASoundActorInteractive::StartLayerChallenge()
{
	if (bPrintDebugInfo)
	{
		GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Cyan, TEXT(__FUNCTION__), true, FVector2D(3.0f, 3.0f));
	}

	FulfillCompSelf->SetFulfilled();
	FulfillCompSelf->UnlockSoundLayer();
	BP_StartLayerChallenge();
	SetStartedChallenge(true);
}

void ASoundActorInteractive::FinishLayerChallenge()
{
	if (bPrintDebugInfo)
	{
		GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Cyan, TEXT(__FUNCTION__), true, FVector2D(3.0f, 3.0f));
	}
	
	BP_FinishLayerChallenge();
}

void ASoundActorInteractive::Reset()
{
	SetNumCorrectHits(0);
}

void ASoundActorInteractive::HandleCorrectInput()
{
	SetNumCorrectHits(GetNumCorrectHits() + 1);

	if (!bHadAllInputs)
	{
		OnCorrectInput.Broadcast();
	}
}


