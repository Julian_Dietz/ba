#pragma once
#include "InstrumentType.h"
#include "FMusicalNote.h"
#include "Components/SceneComponent.h"
#include "BATraceComponent.generated.h"

UENUM(BlueprintType)
enum class EPointDirection : uint8
{
	Invalid,
	Middle,
	LeftTop,
	LeftBottom,
	RightTop,
	RightBottom
};


DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnDistanceChanged, UBATraceComponent*, TraceComponent, bool, bInMinRange);

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnPointedSomewhere, UBATraceComponent*, TraceComponent, EPointDirection, PointDirection);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnStoppedPointing, UBATraceComponent*, TraceComponent);

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnTracePointerEnter, UBATraceComponent*, TraceComponent, AActor*, PointedActor);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnTracePointerLeave, UBATraceComponent*, TraceComponent, AActor*, PreviouslyPointedActor);

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnTraceInputGiven, UBATraceComponent*, TraceComponent, AActor*, InputActor);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnTraceInputReleased, UBATraceComponent*, TraceComponent, AActor*, InputActor);


UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class BA_PROOFCONCEPT_API UBATraceComponent : public USceneComponent
{
	GENERATED_BODY()

public:	
	UBATraceComponent();
	virtual void BeginPlay() override;
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	FVector GetTraceEnd() const;

protected:
	UFUNCTION()
	void HandleAction(float Value);

	UFUNCTION()
	void OnMusicLayerUnlocked(EInstrumentType InstrumentType);

	UFUNCTION(BlueprintCallable, Category = "BA|Gameplay")
	AActor* GetLastPointedActor() const;

	UFUNCTION(BlueprintPure, Category = "BA|Gameplay")
	float GetCurrentInputValue() const
	{
		return CurrentInputValue;
	}

	UFUNCTION()
	void OnBeat(EInstrumentType InInstrumentType, const FMusicalNote& InNextNote);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BA|Debug")
	bool bDrawDebugVisuals;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BA|Gameplay")
	float MinActivationDistance;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BA|Gameplay")
	float InputThreshold;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BA|Gameplay")
	float OnBeatVibrationIntensity;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BA|Gameplay")
	float OnBeatVibrationDuration;

	UPROPERTY(BlueprintAssignable, Category = "BA|Gameplay")
	FOnDistanceChanged OnMinDistanceChanged;

	UPROPERTY(BlueprintAssignable, Category = "BA|Gameplay")
	FOnPointedSomewhere OnPointedSomewhere;

	UPROPERTY(BlueprintAssignable, Category = "BA|Gameplay")
	FOnStoppedPointing OnStoppedPointing;

	UPROPERTY(BlueprintAssignable, Category = "BA|Gameplay")
	FOnTracePointerEnter OnTracePointerEnter;

	UPROPERTY(BlueprintAssignable, Category = "BA|Gameplay")
	FOnTracePointerLeave OnTracePointerLeave;

	UPROPERTY(BlueprintAssignable, Category = "BA|Gameplay")
	FOnTraceInputGiven OnTraceInputGiven;

	UPROPERTY(BlueprintAssignable, Category = "BA|Gameplay")
	FOnTraceInputReleased OnTraceInputReleased;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BA|Gameplay")
	UHapticFeedbackEffect_Base* BeatVibrationEffect;

	TWeakObjectPtr<USceneComponent> ReferenceComponent;

	UFUNCTION()
	void OnCorrectInput();

private:
	void ResetInputActor();
	void ActivateNearestSoundActor();
	void SetupInput();
	void Vibrate(float Intensity);

	bool bHasMinDistance;
	TArray<AActor*> SoundActors;
	TWeakObjectPtr<AActor> LastPointedActor;
	TWeakObjectPtr<AActor> InputActor;
	FVector TraceEnd;

	float CurrentInputValue;
	bool bActionPressed;

	EInstrumentType TypeToVibrate;
};
