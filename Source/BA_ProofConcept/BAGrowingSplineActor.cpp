#include "BA_ProofConcept.h"
#include "BAGrowingSplineActor.h"

#include "NoteManager.h"
#include "Components/SplineComponent.h"
#include "Components/SplineMeshComponent.h"

ABAGrowingSplineActor::ABAGrowingSplineActor():
	SplineComponent(CreateDefaultSubobject<USplineComponent>(TEXT("SplineComponent"))),
	LatestSplineMesh(nullptr),
	GrowInterval(1.0f),
	GrowSpeed(100.0f),
	Emitter(CreateDefaultSubobject<UParticleSystemComponent>(TEXT("Emitter"))),
	LastSplinePointIndex(1)
{
	RootComponent = SplineComponent;
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;
}

void ABAGrowingSplineActor::BeginPlay()
{
	Super::BeginPlay();
	AddSplineMesh(0, 1);
	GetWorldTimerManager().SetTimer(ChangeGrowTimerHandle, this, &ABAGrowingSplineActor::GrowNewHead, GrowInterval, true, GrowInterval);
}

void ABAGrowingSplineActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	UpdateSplineEnd(DeltaTime);
}

void ABAGrowingSplineActor::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);
	GetWorldTimerManager().ClearAllTimersForObject(this);
}

void ABAGrowingSplineActor::OnInputEvaluated(float TimeDifference, EInstrumentType InInstrumentType)
{
	Super::OnInputEvaluated(TimeDifference, InInstrumentType);
}

void ABAGrowingSplineActor::GrowNewHead()
{
	AddSplinePointAtEnd();
	AddSplineMesh(LastSplinePointIndex - 1, LastSplinePointIndex);
}

void ABAGrowingSplineActor::AddSplinePointAtEnd()
{
	FVector Position = SplineComponent->GetLocationAtSplinePoint(LastSplinePointIndex, ESplineCoordinateSpace::Local);
	SplineComponent->AddSplinePoint(Position, ESplineCoordinateSpace::Local);
	++LastSplinePointIndex;
	SplineComponent->SetDefaultUpVector(FVector::UpVector, ESplineCoordinateSpace::World);
}

void ABAGrowingSplineActor::AddSplineMesh(int32 IndexBeginPoint, int32 IndexEndPoint)
{
	GrowDirection = FMath::VRand();
	GrowDirection.Z = 0.8f;
	GrowDirection.Normalize();
	
	FVector MeshLocation, MeshTangent, MeshLocationNext, MeshTangentNext;
	SplineComponent->GetLocationAndTangentAtSplinePoint(IndexBeginPoint, MeshLocation, MeshTangent, ESplineCoordinateSpace::World);
	SplineComponent->GetLocationAndTangentAtSplinePoint(IndexEndPoint, MeshLocationNext, MeshTangentNext, ESplineCoordinateSpace::World);

	const FAttachmentTransformRules AttachmentRules(EAttachmentRule::KeepRelative, true); 
	LatestSplineMesh = NewObject<USplineMeshComponent>(this);
	LatestSplineMesh->AttachToComponent(SplineComponent, AttachmentRules);
	LatestSplineMesh->SetForwardAxis(ESplineMeshAxis::Z);
	LatestSplineMesh->SetStaticMesh(SplineMeshType);
	LatestSplineMesh->SetMaterial(0, SplineMeshMaterial);

	LatestSplineMesh->SetStartAndEnd(MeshLocation, MeshTangent, MeshLocationNext, MeshTangentNext);
	LatestSplineMesh->RegisterComponent();
}

void ABAGrowingSplineActor::UpdateSplineEnd(float DeltaSeconds)
{
	// Spline 
	const FVector OldTopLocation = SplineComponent->GetLocationAtSplinePoint(LastSplinePointIndex, ESplineCoordinateSpace::Local);
	const FVector NewTopLocation = OldTopLocation + GrowDirection * GrowSpeed * DeltaSeconds;
	SplineComponent->SetLocationAtSplinePoint(LastSplinePointIndex, NewTopLocation, ESplineCoordinateSpace::Local);

	// Mesh
	FVector NewMeshEndLocation, NewMeshEndTangent;
	SplineComponent->GetLocationAndTangentAtSplinePoint(LastSplinePointIndex, NewMeshEndLocation, NewMeshEndTangent, ESplineCoordinateSpace::World);
	LatestSplineMesh->SetEndPosition(NewMeshEndLocation);
	LatestSplineMesh->SetEndTangent(NewMeshEndTangent);
}
