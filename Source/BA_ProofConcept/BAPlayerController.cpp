#include "BA_ProofConcept.h"
#include "BAPlayerController.h"

#include "InputAdapterComponent.h"
#include "BATraceComponent.h"

ABAPlayerController::ABAPlayerController():
	InputAdapterComponent(CreateDefaultSubobject<UInputAdapterComponent>("InputAdapterComponent"))
{
	PrimaryActorTick.bCanEverTick = true;
}

void ABAPlayerController::BeginPlay()
{
	Super::BeginPlay();
}

void ABAPlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();

	InputComponent->BindAction(TEXT("Action"), EInputEvent::IE_Pressed, this, &ABAPlayerController::OnActionPressed);
	InputComponent->BindAction(TEXT("Action"), EInputEvent::IE_Repeat, this, &ABAPlayerController::OnActionRepeat);
}

void ABAPlayerController::OnActionPressed()
{
	InputAdapterComponent->GiveInput(EInstrumentType::Kick);
	OnActionButtonPressed();

	if (SpawnedActor.IsValid())
	{
		SpawnedActor->Destroy();
	}

	const FVector Location = GetPawn()->FindComponentByClass<UBATraceComponent>()->GetTraceEnd();
	const FVector* LocPtr = &Location;

	SpawnedActor = GetWorld()->SpawnActorDeferred<AActor>(ObjectTypeToSpawn.Get(), FTransform(Location));
	SpawnedActor->SetActorLocation(Location);
}

void ABAPlayerController::OnActionRepeat()
{
	OnActionButtonRepeat();
	const FVector Location = GetPawn()->FindComponentByClass<UBATraceComponent>()->GetTraceEnd();
	SpawnedActor->SetActorLocation(Location);
}

void ABAPlayerController::OnActionReleased()
{
	OnActionButtonReleased();
	if (SpawnedActor.IsValid())
	{
		//UGameplayStatics::FinishSpawningActor()
	}
}