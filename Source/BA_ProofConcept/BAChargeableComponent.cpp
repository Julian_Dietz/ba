#include "BA_ProofConcept.h"
#include "BAChargeableComponent.h"


UBAChargeableComponent::UBAChargeableComponent():
	bCharge(false),
	ChargeTime(5.0f),
	CurrentChargeTime(0.0f),
	bChargedQuarter(false),
	bChargedHalf(false),
	bChargedThreeQuarter(false)
{
	PrimaryComponentTick.bCanEverTick = true;
}

void UBAChargeableComponent::BeginPlay()
{
	Super::BeginPlay();
}

void UBAChargeableComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	if (bCharge)
	{
		CurrentChargeTime += DeltaTime;
		const float Percent = GetCurrentChargePercentage();

		if (!bChargedQuarter && Percent >= 0.25f && Percent < 0.5f)
		{
			bChargedQuarter = true;
			OnChargedQuarter.Broadcast(this);
		}
		else if (!bChargedHalf && Percent >= 0.5f && Percent < 0.75f)
		{
			bChargedHalf = true;
			OnChargedHalf.Broadcast(this);
		}
		else if (!bChargedThreeQuarter && Percent >= 0.75f && Percent < 1.0f)
		{
			bChargedThreeQuarter = true;
			OnChargedThreeQuarter.Broadcast(this);
		}
		else if (Percent >= 1.0f)
		{
			OnChargedFull.Broadcast(this);
			SetComponentTickEnabled(false);
		}
	}
}

float UBAChargeableComponent::GetCurrentChargePercentage() const
{
	return FMath::Min(1.0f, (CurrentChargeTime / ChargeTime));
}

