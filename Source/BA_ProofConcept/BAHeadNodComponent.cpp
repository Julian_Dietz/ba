#include "BA_ProofConcept.h"
#include "BAHeadNodComponent.h"

#include "NoteManager.h"
#include "InputAdapterComponent.h"

UBAHeadNodComponent::UBAHeadNodComponent():
	InstrumentType(EInstrumentType::Invalid),
	MinimumNodAmplitude(0.060f),
	MaximumZOffsetOnBeat(0.045f),
	bTrackHeadMotion(true),
	MaxZOffset(0.0f),
	CurrentZOffset(0.0f),
	CurrentOrientation(0.0f),
	ComponentToTrack(nullptr)
{
	PrimaryComponentTick.bCanEverTick = true;
}

void UBAHeadNodComponent::BeginPlay()
{
	Super::BeginPlay();
	for (TActorIterator<ANoteManager> NoteManager(GetWorld()); NoteManager; ++NoteManager)
	{
		NoteManager->OnBeat.AddDynamic(this, &UBAHeadNodComponent::OnBeat);
	}
} 

void UBAHeadNodComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	if (bTrackHeadMotion)
	{
		float Offset = FVector::DotProduct(GetAttachParent()->GetComponentRotation().Vector(), FVector::UpVector);

		CurrentZOffset = FMath::Abs(Offset - CurrentOrientation);
		MaxZOffset = CurrentZOffset > MaxZOffset ? CurrentZOffset : MaxZOffset;
	}
}

void UBAHeadNodComponent::OnBeat(EInstrumentType InInstrumentType, const FMusicalNote& InNextNote)
{
	if (MaxZOffset >= MinimumNodAmplitude && CurrentZOffset <= MaximumZOffsetOnBeat)
	{
		auto PlayerController = GetWorld()->GetFirstPlayerController();
		ensure(PlayerController->GetComponentByClass(UInputAdapterComponent::StaticClass()));

		auto InputAdapter = static_cast<UInputAdapterComponent*>(PlayerController->GetComponentByClass(UInputAdapterComponent::StaticClass()));
		InputAdapter->GiveInput(InstrumentType);
	}

	CurrentOrientation = FVector::DotProduct(GetAttachParent()->GetComponentRotation().Vector(), FVector::UpVector);
	CurrentZOffset = 0.0f;
	MaxZOffset = 0.0f;
}


