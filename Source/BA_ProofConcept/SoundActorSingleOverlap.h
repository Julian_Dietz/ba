#pragma once
#include "SoundActorInteractive.h"
#include "SoundActorSingleOverlap.generated.h"

UCLASS()
class BA_PROOFCONCEPT_API ASoundActorSingleOverlap : public ASoundActorInteractive
{
	GENERATED_BODY()

public:
	ASoundActorSingleOverlap();

	virtual void BeginPlay() override;
protected:
	UFUNCTION()
	virtual void OnNoteStateChanged(EInstrumentType InInstrumentType, int32 NoteThatChangedIndex, ENoteState NewNoteState) override;
	UFUNCTION()
	virtual void OnBeginOverlap(AActor* OverlappedActor, AActor* OtherActor);
	UFUNCTION()
	virtual void OnComponentBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "BA|Gameplay")
	bool bConstrain;
};
