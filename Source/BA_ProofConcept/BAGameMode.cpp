#include "BA_ProofConcept.h"
#include "BAGameMode.h"

void ABAGameMode::InitGame(const FString& MapName, const FString& Options, FString& ErrorMessage)
{
	ensure(VRPawnClass.Get());
	ensure(NonVRPawnClass.Get());
	DefaultPawnClass = GEngine->HMDDevice.Get() ? VRPawnClass : NonVRPawnClass;

	Super::InitGame(MapName, Options, ErrorMessage);
}

