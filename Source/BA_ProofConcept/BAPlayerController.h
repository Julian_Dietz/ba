#pragma once
#include "GameFramework/PlayerController.h"
#include "BAPlayerController.generated.h"

UCLASS()
class BA_PROOFCONCEPT_API ABAPlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	ABAPlayerController();
	virtual void BeginPlay() override;

protected:
	virtual void SetupInputComponent() override;

	UFUNCTION(BlueprintImplementableEvent, Category="Input")
	void OnActionButtonPressed();
	UFUNCTION(BlueprintImplementableEvent, Category = "Input")
	void OnActionButtonRepeat();
	UFUNCTION(BlueprintImplementableEvent, Category = "Input")
	void OnActionButtonReleased();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BA")
	TSubclassOf<AActor> ObjectTypeToSpawn;

private:
	UFUNCTION()
	void OnActionPressed();
	UFUNCTION()
	void OnActionRepeat();
	UFUNCTION()
	void OnActionReleased();

	class UInputAdapterComponent* InputAdapterComponent;
	TWeakObjectPtr<AActor> SpawnedActor;
};
