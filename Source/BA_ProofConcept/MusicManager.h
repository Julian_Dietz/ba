#pragma once
#include "FMusicalNote.h"
#include "InstrumentType.h"
#include "GameFramework/Actor.h"
#include "MusicManager.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnMusicLayerUnlocked, EInstrumentType, InstrumentType);

UCLASS()
class BA_PROOFCONCEPT_API AMusicManager : public AActor
{
	GENERATED_BODY()
	
public:	
	AMusicManager();
	virtual void BeginPlay() override;

	UPROPERTY(BlueprintAssignable, Category = "Gameplay")
	FOnMusicLayerUnlocked OnMusicLayerUnlocked;

protected:
	UFUNCTION(BlueprintImplementableEvent, Category = "Gameplay")
	void BP_OnBeat();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Gameplay")
	TMap<EInstrumentType, USoundBase*> MusicLayers;

	UFUNCTION()
	void UnlockMusicLayer(EInstrumentType InstrumentType);

	UFUNCTION()
	void OnChallengesFulfilled();

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "BA|Music")
	USoundBase* UnlockSound;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "BA|Music")
	USoundBase* PartySound;

private:
	UFUNCTION()
	void OnBeat(EInstrumentType InInstrumentType, const FMusicalNote& InNextNote);

	TMap<EInstrumentType, UAudioComponent*> AudioComponents;
	TArray<EInstrumentType> UnlockedMusicLayers;

	UAudioComponent* UnlockSoundAudioComp;

	bool bChallengesFulfilled;
};
