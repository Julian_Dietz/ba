#include "BA_ProofConcept.h"
#include "GameFlowManager.h"

#include "SoundActorInteractive.h"

AGameFlowManager::AGameFlowManager() :
	DelayUntilMusic(3.0f),
	Index(0)
{
	PrimaryActorTick.bCanEverTick = false;
}

void AGameFlowManager::BeginPlay()
{
	Index = DebugStartIndex;
	Super::BeginPlay();

	for (auto ActorPair : SoundActors)
	{
		if (ActorPair.Value)
		{
			ActorPair.Value->OnGaveEnoughInputsOnSoundActor.AddDynamic(this, &AGameFlowManager::OnGaveEnoughInputsOnSoundActor);
		}
	}

	StartNotesDelayed();
}

void AGameFlowManager::OnGaveEnoughInputsOnSoundActor(AActor* SoundActor)
{
	(*SoundActors.Find(Index))->FinishLayerChallenge();
	ASoundActorInteractive** SoundActorPtr = SoundActors.Find(++Index);

	if (!SoundActorPtr)
	{
		OnChallengesFulfilled.Broadcast();
		GetWorldTimerManager().SetTimer(StartMusicTimerHandle, this, &AGameFlowManager::EndGame, 1.0f, false, 15.0f);
	}
	else
	{
		(*SoundActorPtr)->StartLayerChallenge();
	}
}

void AGameFlowManager::StartNotesDelayed()
{
	GetWorldTimerManager().SetTimer(StartMusicTimerHandle, this, &AGameFlowManager::StartNotes, 1.0f, false, DelayUntilMusic);
}

void AGameFlowManager::StartNotes()
{
	OnNotesStarted.Broadcast();
	if (auto ActorPair = SoundActors.Find(Index)) // debug, remove
	{
		(*ActorPair)->StartLayerChallenge();
	}
}

void AGameFlowManager::EndGame()
{
	OnGameEnded.Broadcast();
}