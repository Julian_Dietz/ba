#include "BA_ProofConcept.h"
#include "SoundActorSingleOverlap.h"

#include "BAMusicalLibrary.h"
#include "BAPawn.h"


ASoundActorSingleOverlap::ASoundActorSingleOverlap():
	bConstrain(true)
{
}

void ASoundActorSingleOverlap::BeginPlay()
{
	Super::BeginPlay();
	OnActorBeginOverlap.AddDynamic(this, &ASoundActorSingleOverlap::OnBeginOverlap);

	if (bConstrain)
	{
		UBoxComponent* BoxComp = FindComponentByClass<UBoxComponent>();
		ensure(BoxComp);
		if (BoxComp)
		{
			BAMusicalLibrary::SetupConstraint(this, BoxComp);
			BoxComp->OnComponentBeginOverlap.AddDynamic(this, &ASoundActorSingleOverlap::OnComponentBeginOverlap);
		}
	}
}

void ASoundActorSingleOverlap::OnBeginOverlap(AActor* OverlappedActor, AActor* OtherActor)
{
	if (bStartedChallenge)
	{
		if (OtherActor->IsA<ABAPawn>())
		{
			if (NoteStateOnInput == ENoteState::Invalid)
			{
				NoteStateOnInput = CurrentNoteState;
				OnInput(NoteStateOnInput);
				if (NoteStateOnInput == ENoteState::OnBeat)
				{
					HandleCorrectInput();
				}
			}
		}
	}
}

void ASoundActorSingleOverlap::OnNoteStateChanged(EInstrumentType InInstrumentType, int32 NoteThatChangedIndex, ENoteState NewNoteState)
{
	Super::OnNoteStateChanged(InInstrumentType, NoteThatChangedIndex, NewNoteState);
	
	if (bStartedChallenge)
	{
		if (InInstrumentType == InstrumentType)
		{
			CurrentNoteState = NewNoteState;

			if (CurrentNoteState == ENoteState::Ended)
			{
				NoteStateOnInput = ENoteState::Invalid;
			}
		}
	}
}

void ASoundActorSingleOverlap::OnComponentBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (bStartedChallenge)
	{
		if (OtherActor->IsA<ABAPawn>())
		{
			const FVector ImpulseDirection = (OverlappedComp->GetComponentLocation() - OtherComp->GetComponentLocation()).GetUnsafeNormal();
			const float ImpulseStrength = 250.0f;

			OverlappedComp->AddImpulse(ImpulseDirection * ImpulseStrength, NAME_None, true);
		}
	}
}