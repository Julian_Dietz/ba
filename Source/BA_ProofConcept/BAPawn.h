#pragma once

#include "GameFramework/Pawn.h"
#include "BAPawn.generated.h"

UCLASS()
class BA_PROOFCONCEPT_API ABAPawn : public APawn
{
	GENERATED_BODY()

public:
	ABAPawn();

	virtual void BeginPlay() override;
	virtual void Tick(float DeltaTime) override;
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	USceneComponent* MyRootComponent;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UCapsuleComponent* CapsuleComponent;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UCameraComponent* CameraComponent;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	USceneComponent* SceneComponent;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	class UMotionControllerComponent* MCComponentLeft;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	class UMotionControllerComponent* MCComponentRight;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	class USteamVRChaperoneComponent* SteamVRChaperoneComponent;

	UFUNCTION()
	void OnGameEnded();

	UFUNCTION(BlueprintImplementableEvent, Category="BA|Gameplay")
	void BP_OnGameEnded();
};
