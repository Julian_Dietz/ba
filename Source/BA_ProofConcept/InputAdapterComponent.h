// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "InstrumentType.h"
#include "Components/ActorComponent.h"
#include "InputAdapterComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnInputGiven, EInstrumentType, InInstrumentType);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class BA_PROOFCONCEPT_API UInputAdapterComponent : public UActorComponent
{
	GENERATED_BODY()
public:	
	UInputAdapterComponent();

	virtual void BeginPlay() override;
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	void GiveInput(EInstrumentType InInstrumentType) const;

	FOnInputGiven OnInputGiven;
};
