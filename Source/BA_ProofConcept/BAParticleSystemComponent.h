#pragma once
#include "Particles/ParticleSystemComponent.h"
#include "BAParticleSystemComponent.generated.h"

UCLASS(ClassGroup = (Rendering, Common), hidecategories = Object, hidecategories = Physics, hidecategories = Collision, showcategories = Trigger, editinlinenew, meta = (BlueprintSpawnableComponent))
class BA_PROOFCONCEPT_API UBAParticleSystemComponent : public UParticleSystemComponent
{
	GENERATED_BODY()
	
public:
	UFUNCTION(BlueprintCallable, Category="ParticleComponent")
	void SetParticleVelocities(FName EmitterName, FVector NewVelocity, bool bRandomizePerParticle, float SpeedRandom);
	
	UFUNCTION(BlueprintCallable, Category = "ParticleComponent")
	void BoostParticleVelocities(FName EmitterName, float Amount);

private:
	void SetEmitterParticleVelocities(FParticleEmitterInstance* Emitter, FVector NewVelocity, bool bRandomizePerParticle, float SpeedRandom);
	void BoostEmitterParticleVelocities(FParticleEmitterInstance* Emitter, float Amount);
};
