#include "BA_ProofConcept.h"
#include "InteractiveSoundActorManager.h"

#include "SoundActorSingleoverlapSupp.h"
#include "BAFulfillComponent.h"


AInteractiveSoundActorManager::AInteractiveSoundActorManager() :
	FulfillComp0(CreateDefaultSubobject<UBAFulfillComponent>(TEXT("Fulfill_Clap"))), 
	FulfillComp1(CreateDefaultSubobject<UBAFulfillComponent>(TEXT("Fulfill_Hihat"))),
	NumTPArraysToUse(0),
	ActiveActors(nullptr),
	bHadInput(false),
	CountCurrentBeatEnding(0)
{
	//FulfillComp0->InstrumentType = EInstrumentType::Clap;
	//FulfillComp1->InstrumentType = EInstrumentType::Hihat;
}

void AInteractiveSoundActorManager::Tick(float DeltaTime)
{
	if (bStartedChallenge)
	{
		if (!bHadInput && ActiveActors)
		{
			ENoteState StateOnInput = ENoteState::Invalid;
			for (const auto& Actor : *ActiveActors)
			{
				const ENoteState InputState = Actor->GetNoteStateOnInput();
				StateOnInput = InputState > StateOnInput ? InputState : StateOnInput;
			}

			if (StateOnInput == ENoteState::OnBeat)
			{
				bHadInput = true;
				HandleCorrectInput();
			}
		}

		TryReorderActors(DeltaTime);
	}
}

// This is embarassing, but no time
void AInteractiveSoundActorManager::StartLayerChallenge()
{
	FulfillComp0->SetFulfilled();
	FulfillComp0->UnlockSoundLayer();
	FulfillComp1->SetFulfilled();
	FulfillComp1->UnlockSoundLayer();

	bStartedChallenge = true;

	for (int32 i = 0; i < Actors12.Num(); ++i)
	{
		Actors12[i]->StartFadingIn();
	}

	FTimerHandle Handle;
	GetWorldTimerManager().SetTimer(Handle, this, &AInteractiveSoundActorManager::StartFadingDelayed, 1.0f);

	bStartedChallenge = true;
}

void AInteractiveSoundActorManager::FinishLayerChallenge()
{
	Super::FinishLayerChallenge();
	SetActorTickEnabled(false);
}

void AInteractiveSoundActorManager::OnNoteStateChanged(EInstrumentType InInstrumentType, int32 NoteThatChangedIndex, ENoteState NewNoteState)
{
	if (bStartedChallenge)
	{
		if (InInstrumentType == Actors12[0]->GetInstrumentType())
		{
			if (NewNoteState == ENoteState::PreBeat)
			{
				ActiveActors = &Actors12;
			}
			else if (NewNoteState == ENoteState::PostBeat)
			{
				ActiveActors = nullptr;
				bHadInput = false;
			}
		}
		else if (InInstrumentType == Actors3[0]->GetInstrumentType())
		{
			if (NewNoteState == ENoteState::PreBeat)
			{
				ActiveActors = &Actors3;
		}	
			else if (NewNoteState == ENoteState::PostBeat)
			{
				ActiveActors = nullptr;
				bHadInput = false;
			}
			else if (NewNoteState == ENoteState::Ended)
			{
				SetUpReorderingOfActors();
			}
		}
	}
}

void AInteractiveSoundActorManager::StartFadingDelayed()
{
	for (int32 i = 0; i < Actors3.Num(); ++i)
	{
		Actors3[i]->StartFadingIn();
	}
}

void AInteractiveSoundActorManager::SetUpReorderingOfActors()
{
	if (NumTPArraysToUse > 0)
	{
		TArray<ATargetPoint*> TargetPoints = GetCurrentTargetPoints();
	
		LerpActors.Add(Actors12[0], TargetPoints[0]);
		LerpActors.Add(Actors12[1], TargetPoints[1]);
		LerpActors.Add(Actors3[0], TargetPoints[2]);
		LerpActors.Add(Actors3[1], TargetPoints[3]);

		CountCurrentBeatEnding = (CountCurrentBeatEnding + 1) % NumTPArraysToUse;
	}
}

void AInteractiveSoundActorManager::TryReorderActors(const float DeltaTime)
{
	for (auto& LerpPair : LerpActors)
	{
		if (LerpPair.Key)
		{
			const FVector StartPos = LerpPair.Key->GetActorLocation();
			const FVector EndPos = LerpPair.Value->GetActorLocation();
			const FVector NewLoc = FMath::VInterpTo(StartPos, EndPos, GetWorld()->GetDeltaSeconds(), 5.0f);

			LerpPair.Key->SetActorLocation(NewLoc);

			if (NewLoc.Equals(EndPos))
			{
				LerpActors.Remove(LerpPair.Key);
			}
		}
	}
}

TArray<ATargetPoint*>& AInteractiveSoundActorManager::GetCurrentTargetPoints()
{
	ensure(CountCurrentBeatEnding < NumTPArraysToUse && CountCurrentBeatEnding > -1);

	switch (CountCurrentBeatEnding)
	{
		case 0:
			return TargetPointsBeat1;
		case 1:
			return TargetPointsBeat2;
		case 2:
			return TargetPointsBeat3;
		case 3:
			return TargetPointsBeat4;
		default:
			return TargetPointsBeat1;
	}
}
