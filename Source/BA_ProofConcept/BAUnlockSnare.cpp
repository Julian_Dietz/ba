#include "BA_ProofConcept.h"
#include "BAUnlockSnare.h"

#include "BAPawn.h"
#include "BAMusicalLibrary.h"
#include "MotionControllerComponent.h"
#include "NoteManager.h"

ABAUnlockSnare::ABAUnlockSnare():
	MaxDistance(75.0f),
	PreviousNote(FMusicalNote()),
	NextNote(FMusicalNote()),
	CurrentDistance(0.0f),
	bClapped(false),
	bActiveObjective(false)
{
}

void ABAUnlockSnare::BeginPlay()
{
	Super::BeginPlay();
	for (TActorIterator<ABAPawn> PlayerPawn(GetWorld()); PlayerPawn; ++PlayerPawn)
	{
		auto Comps = PlayerPawn->GetComponentsByClass(UMotionControllerComponent::StaticClass());
		ensure(Comps.Num() == 2);

		for (auto Comp : Comps)
		{
			MotionControllerComps.Add(static_cast<UMotionControllerComponent*>(Comp));
		}

		/*TArray<UBoxComponent*> Boxes = */
		auto BoxComponents = PlayerPawn->GetComponentsByClass(UBoxComponent::StaticClass());
		static_cast<UBoxComponent*>(BoxComponents[0])->OnComponentBeginOverlap.AddDynamic(this, &ABAUnlockSnare::OnClap);
	}

	for (TActorIterator<ANoteManager> ItNoteManager(GetWorld()); ItNoteManager; ++ItNoteManager)
	{
		NoteManager = *ItNoteManager;
	}
}

void ABAUnlockSnare::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	// Distance 
	CurrentDistance = (MotionControllerComps[0]->GetComponentLocation() - MotionControllerComps[1]->GetComponentLocation()).Size();
	if (bClapped && CurrentDistance > 25.0f)
	{
		bClapped = false;
	}
}

void ABAUnlockSnare::OnBeat(EInstrumentType InInstrumentType, const FMusicalNote& InNextNote)
{
	Super::OnBeat(InInstrumentType, InNextNote);
	if (InInstrumentType == InstrumentType)
	{
		PreviousNote = InNextNote;
		NextNote = NoteManager->GetNextNote(InstrumentType);
	}
}

void ABAUnlockSnare::RefreshControllerRumble()
{
	if (!bClapped)
	{
		APlayerController* PC = GetWorld()->GetFirstPlayerController();

		const float VibrationIntensity = FMath::Max(1.0f - (CurrentDistance / MaxDistance), 0.0f) * 0.25f;
		PC->PlayHapticEffect(BeatVibrationEffect, EControllerHand::Left, VibrationIntensity);
		PC->PlayHapticEffect(BeatVibrationEffect, EControllerHand::Right, VibrationIntensity);
	}
}

void ABAUnlockSnare::StartVibrating()
{
	GetWorldTimerManager().SetTimer(RefreshVibrateTimerHandle, this, &ABAUnlockSnare::RefreshControllerRumble, 0.25f, true, 0.0f);
}

void ABAUnlockSnare::StopVibrating()
{
	GetWorldTimerManager().ClearTimer(RefreshVibrateTimerHandle);
}

void ABAUnlockSnare::FulfillObjective()
{
	for (TActorIterator<ABAPawn> PlayerPawn(GetWorld()); PlayerPawn; ++PlayerPawn)
	{
		auto BoxComponents = PlayerPawn->GetComponentsByClass(UBoxComponent::StaticClass());
		static_cast<UBoxComponent*>(BoxComponents[0])->OnComponentBeginOverlap.RemoveDynamic(this, &ABAUnlockSnare::OnClap);
		StopVibrating();
	}
}

void ABAUnlockSnare::EnableObjective()
{
	bActiveObjective = true;
	StartVibrating();
}

void ABAUnlockSnare::OnClap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	float TimeDifference;
	const float CurrentTime = GetWorld()->TimeSeconds;

	if (bActiveObjective && OtherComp->ComponentHasTag(FName(TEXT("ClapVolume")))
		&& BAMusicalLibrary::EvaluateInput(TimeDifference, CurrentTime, &PreviousNote, &NextNote))
	{
		if (FMath::Abs(TimeDifference) < 0.3f && !bClapped)
		{
			bClapped = true;
			OnClapOnBeat();
		}
	}
}