#pragma once
#include "SoundActorSingleOverlap.h"
#include "SoundActorSingleOverlapSupp.generated.h"

UCLASS()
class BA_PROOFCONCEPT_API ASoundActorSingleOverlapSupp : public ASoundActorSingleOverlap
{
	GENERATED_BODY()
	
public:
	friend class AInteractiveSoundActorManager;

	ASoundActorSingleOverlapSupp();
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaTime) override;

	FORCEINLINE ENoteState GetNoteStateOnInput() const
	{
		return NoteStateOnInput;
	}

	void StartFadingIn();

protected:
	UFUNCTION()
	virtual void OnBeginOverlap(AActor* OverlappedActor, AActor* OtherActor);

	UFUNCTION()
	virtual void OnNoteStateChanged(EInstrumentType InInstrumentType, int32 NoteThatChangedIndex, ENoteState NewNoteState) override;

	bool bFadingIn;
};
