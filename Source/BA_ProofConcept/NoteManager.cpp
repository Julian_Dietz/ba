#include "BA_ProofConcept.h"
#include "NoteManager.h"

#include "BAFileLibrary.h"
#include "BAMusicalLibrary.h"
#include "GameFlowManager.h"
#include "MusicDataAsset.h"
#include "BAFulfillComponent.h"

static const FString FilePath = TEXT("");

ANoteManager::ANoteManager() :
	BPM(130.0f),
	TimeMusicLoopStarted(0),
	EnqueueCount(2)
{
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = false;

	ActiveMusicLayersBuffer.Add(TMap<EInstrumentType, TArray<FMusicalNote, TInlineAllocator<256>>>());
	ActiveMusicLayersBuffer.Add(TMap<EInstrumentType, TArray<FMusicalNote, TInlineAllocator<256>>>());
}

void ANoteManager::PreInitializeComponents()
{
	Super::PreInitializeComponents();

	CompileNotes();
}

void ANoteManager::BeginPlay()
{
	Super::BeginPlay();

	for (TActorIterator<AGameFlowManager> GameFlowManager(GetWorld()); GameFlowManager; ++GameFlowManager)
	{
		GameFlowManager->OnNotesStarted.AddDynamic(this, &ANoteManager::OnNotesStarted);
	}

	for (TActorIterator<AActor> Actor(GetWorld()); Actor; ++Actor)
	{
		auto Comps = Actor->GetComponentsByClass(UBAFulfillComponent::StaticClass());
		for(auto Comp : Comps)
		{
			static_cast<UBAFulfillComponent*>(Comp)->OnObjectiveFulfilled.AddDynamic(this, &ANoteManager::OnMusicLayerUnlocked);
		}
	}
}

void ANoteManager::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	const float CurrentTime = GetWorld()->TimeSeconds;
	HandleBeat(CurrentTime);
}

FMusicalNote ANoteManager::GetNextNote(EInstrumentType InstrumentType) const
{
	auto Notes = ActiveMusicLayersBuffer[0][InstrumentType];
	if (Notes.Num() > 1)
	{
		return Notes[Notes.Num() - 2];
	}
	else
	{
		// TODO: if needed, make this work without the assumption that offsets between beats are 
		// the same. I'd suggest to cache the total time per notelayer, but don't have time to do so right now
		auto OriginalNotes = MusicLayers[InstrumentType];
		const float AdditionalOffset = OriginalNotes[0].TimeStamp - OriginalNotes[1].TimeStamp;
		FMusicalNote Note = MusicLayers[InstrumentType].Top();
		Note.TimeStamp += GetWorld()->TimeSeconds + AdditionalOffset;
		return Note;
	}
}

FMusicalNote* ANoteManager::TryGetActiveNoteByIndex(EInstrumentType InInstrumentType, int32 InNoteIndex)
{
	for (int32 BufferIndex = 0; BufferIndex < ActiveMusicLayersBuffer.Num(); ++BufferIndex)
	{
		auto& CurrentNotes = ActiveMusicLayersBuffer[BufferIndex];
		auto InstrumentLayer = *CurrentNotes.Find(InInstrumentType);
		FMusicalNote* Note = InstrumentLayer.FindByPredicate([InNoteIndex](const FMusicalNote& Note)
		{
			return Note.NoteIndex == InNoteIndex && Note.GetCurrentNoteState() != ENoteState::Invalid;
		});

		if (Note)
		{
			return Note;
		}
	}

	return nullptr;
}

void ANoteManager::OnNotesStarted()
{
	TimeMusicLoopStarted = GetWorld()->TimeSeconds;
	ResetAllNotes();
	SetActorTickEnabled(true);
}

void ANoteManager::OnMusicLayerUnlocked(EInstrumentType InstrumentType)
{
	for (int32 BufferIndex = 0; BufferIndex < ActiveMusicLayersBuffer.Num(); ++BufferIndex)
	{
		auto& CurrentNoteLayer = ActiveMusicLayersBuffer[BufferIndex];
		ensure(CurrentNoteLayer.Find(InstrumentType) == nullptr);
		ensure(MusicLayers.Find(InstrumentType));

		if (CurrentNoteLayer.Find(InstrumentType) == nullptr)
		{
			CurrentNoteLayer.Add(InstrumentType, *MusicLayers.Find(InstrumentType));
			auto tim = GetWorld()->TimeSeconds;
			const float Time = TimeMusicLoopStarted +  // Assumes there will always be only 2 SoundBuffers
				BAFileLibrary::GetTimeUntilLoop() * (EnqueueCount - 2 + (EnqueueCount % 2 == 0 ? BufferIndex : (1 - BufferIndex)));
			UpdateNoteLayer(*CurrentNoteLayer.Find(InstrumentType), Time);

			if (IsActorTickEnabled())
			{
				BAMusicalLibrary::StripPlayedNotes(*CurrentNoteLayer.Find(InstrumentType), GetWorld()->TimeSeconds);
			}
		}
	}

	OnNoteLayerUnlocked.Broadcast(InstrumentType);
}

void ANoteManager::UpdateNotes(float TimeToUpdateWith, TMap<EInstrumentType, TArray<FMusicalNote, TInlineAllocator<256>>>& NotesToUpdate)
{
	for (auto& NotePair : NotesToUpdate)
	{
		for (auto& Note : NotePair.Value)
		{
			Note.TimeStamp += TimeToUpdateWith;
		}
	}
}

void ANoteManager::UpdateNoteLayer(TArray<FMusicalNote, TInlineAllocator<256>>& Layer, float TimeToUpdateWith)
{
	for (auto& Note : Layer)
	{
		Note.TimeStamp += TimeToUpdateWith;
	}
}

// Makes the assumption to only get called after a beat is completed
void ANoteManager::ResetAllNotes()
{
	TimeMusicLoopStarted = GetWorld()->TimeSeconds;
	for (int32 BufferIndex = 0; BufferIndex < ActiveMusicLayersBuffer.Num(); ++BufferIndex)
	{
		auto& CurrentNotes = ActiveMusicLayersBuffer[BufferIndex];

		for (auto& NotePair : CurrentNotes)
		{
			NotePair.Value = *MusicLayers.Find(NotePair.Key);
		}

		const float Time = TimeMusicLoopStarted + BAFileLibrary::GetTimeUntilLoop() * BufferIndex;
		UpdateNotes(Time, CurrentNotes);
	}
	Tick(0.0f);
}


void ANoteManager::EnqueueNoteBuffer(TMap<EInstrumentType, TArray<FMusicalNote, TInlineAllocator<256>>>& NotesToUpdate)
{
	for (auto& NotePair : NotesToUpdate)
	{
		NotePair.Value = *MusicLayers.Find(NotePair.Key);
	}
	
	const float Time = TimeMusicLoopStarted + BAFileLibrary::GetTimeUntilLoop() * EnqueueCount++;
	UpdateNotes(Time, NotesToUpdate);
}

void ANoteManager::HandleBeat(float CurrentTime)
{
	for (int32 BufferIndex = 0; BufferIndex < ActiveMusicLayersBuffer.Num(); ++BufferIndex)
	{
		auto& CurrentNotes = ActiveMusicLayersBuffer[BufferIndex];
		for (auto& NotePair : CurrentNotes)
		{
			const bool bLoopFinished = !NotePair.Value.ContainsByPredicate([](const FMusicalNote& Note)
			{
				return !Note.bPlayedBeatYet || Note.GetCurrentNoteState() != ENoteState::Invalid;
			});
			if (bLoopFinished)
			{
				const float LoopTimeStamp = TimeMusicLoopStarted + BAFileLibrary::GetTimeUntilLoop() * (EnqueueCount - 1);
				if (CurrentTime > LoopTimeStamp)
				{
					if (bLoop)
					{
						OnLoop.Broadcast();
						EnqueueNoteBuffer(CurrentNotes); // Set Time Music Loop Started here
						GEngine->AddOnScreenDebugMessage(-1, 3.0f, FColor::Cyan, TEXT("Looped"));
						break;
					}
					else
					{
						SetActorTickEnabled(false);
						OnLastNotePlayed.Broadcast();
						return;
					}
				}
			}
			else
			{
				// There can only be one next beat note! We won't have two notes in the same frame 
				// in a single instrument layer.
				FMusicalNote* BeatNote = nullptr;

				for (int32 Index = NotePair.Value.Num() - 1; Index >= 0; --Index)
				{
					FMusicalNote& Note = NotePair.Value[Index];
					if (!Note.bPlayedBeatYet && !BeatNote)  // Also takes notes that are one loop offset away
					{
						BeatNote = &Note;
					}

					if (Note.UpdateNoteState(CurrentTime))
					{
						const ENoteState NewState = Note.GetCurrentNoteState();
						OnNoteStateChanged.Broadcast(NotePair.Key, Note.NoteIndex, NewState);

						if (NewState == ENoteState::Invalid)
						{
							NotePair.Value.Pop();
						}
					}
					else if (Note.GetCurrentNoteState() == ENoteState::Invalid &&
						!Note.UpdateNoteState(CurrentTime))
					{
						break;
					}
				}

				if (BeatNote && BeatNote->TimeStamp <= CurrentTime)
				{
					ensure(!BeatNote->bPlayedBeatYet);
					BeatNote->bPlayedBeatYet = true;
					OnBeat.Broadcast(NotePair.Key, *BeatNote);

					//if (NotePair.Key == EInstrumentType::Kick)
					//{
					//	FString DebugText = "Kick " + FString::FromInt(NotePair.Value.Num());
					//	GEngine->AddOnScreenDebugMessage(-1, 3.0f, FColor::Red, DebugText, true, FVector2D(3.0f, 3.0f));
					//}
					//if (NotePair.Key == EInstrumentType::DeepSynth)
					//{
					//	FString DebugText = "DeepSynth " + FString::FromInt(NotePair.Value.Num());
					//	GEngine->AddOnScreenDebugMessage(-1, 3.0f, FColor::Red, DebugText);
					//}
					//if (NotePair.Key == EInstrumentType::Snare)
					//{
					//	FString DebugText = "Snare " + FString::FromInt(NotePair.Value.Num());
					//	GEngine->AddOnScreenDebugMessage(-1, 3.0f, FColor::Red, DebugText);
					//}
				}
			}
		}
	}
}

void ANoteManager::CompileNotes()
{
	for (const auto& Pair : NoteFileNames)
	{
		MusicLayers.Add(Pair.Key, BAFileLibrary::GetCompiledNotesAsQueue(FString(FilePath + Pair.Value), BPM));
	}
}

