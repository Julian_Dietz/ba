#include "BA_ProofConcept.h"
#include "MusicManager.h"

#include "BAFulfillComponent.h"
#include "GameFlowManager.h"
#include "NoteManager.h"

AMusicManager::AMusicManager():
	UnlockSound(nullptr),
	UnlockSoundAudioComp(CreateDefaultSubobject<UAudioComponent>(TEXT("Audio_Unlock"))),
	bChallengesFulfilled(false)
{
	PrimaryActorTick.bCanEverTick = false;
	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	AudioComponents.Add(EInstrumentType::Kick, CreateDefaultSubobject<UAudioComponent>(TEXT("Audio_BassDrum")));
	AudioComponents.Add(EInstrumentType::Snare, CreateDefaultSubobject<UAudioComponent>(TEXT("Audio_Snare")));
	AudioComponents.Add(EInstrumentType::Melody, CreateDefaultSubobject<UAudioComponent>(TEXT("Audio_Melody")));
	AudioComponents.Add(EInstrumentType::Bubbles, CreateDefaultSubobject<UAudioComponent>(TEXT("Audio_Bubbles")));
	AudioComponents.Add(EInstrumentType::DeepSynth, CreateDefaultSubobject<UAudioComponent>(TEXT("Audio_DeepSynth")));
	AudioComponents.Add(EInstrumentType::HighSynth, CreateDefaultSubobject<UAudioComponent>(TEXT("Audio_HighSynth")));
	AudioComponents.Add(EInstrumentType::HighSynthExtended, CreateDefaultSubobject<UAudioComponent>(TEXT("Audio_HighSynthExtended")));
	AudioComponents.Add(EInstrumentType::Clap, CreateDefaultSubobject<UAudioComponent>(TEXT("Audio_Clap")));
	AudioComponents.Add(EInstrumentType::Hihat, CreateDefaultSubobject<UAudioComponent>(TEXT("Audio_Hihat")));
}

void AMusicManager::BeginPlay()
{
	Super::BeginPlay();
	for (TActorIterator<ANoteManager> NoteManager(GetWorld()); NoteManager; ++NoteManager)
	{
		NoteManager->OnBeat.AddDynamic(this, &AMusicManager::OnBeat);
	}

	for (auto& Pair : AudioComponents)
	{
		ensure(MusicLayers.Find(Pair.Key) != nullptr);
		if (MusicLayers.Find(Pair.Key) != nullptr)		// debug, remove
		{
			if (USoundBase* Sound = *MusicLayers.Find(Pair.Key))
			{
				Pair.Value->SetSound(Sound);
			}
		}	
	}

	for (TActorIterator<AActor> Actor(GetWorld()); Actor; ++Actor)
	{
		auto Comps = Actor->GetComponentsByClass(UBAFulfillComponent::StaticClass());
		for (auto Comp : Comps)
		{
			static_cast<UBAFulfillComponent*>(Comp)->OnSoundUnlocked.AddDynamic(this, &AMusicManager::UnlockMusicLayer);
		}
	}


	for (TActorIterator<AGameFlowManager> GameFlowManager(GetWorld()); GameFlowManager; ++GameFlowManager)
	{
		GameFlowManager->OnChallengesFulfilled.AddDynamic(this, &AMusicManager::OnChallengesFulfilled);
	}
}

void AMusicManager::UnlockMusicLayer(EInstrumentType InstrumentType)
{
	UnlockedMusicLayers.Add(InstrumentType);
	OnMusicLayerUnlocked.Broadcast(InstrumentType);
	if (UnlockSound && GetWorld()->TimeSeconds > 5.0f)
	{
		UnlockSoundAudioComp->SetSound(UnlockSound);
		UnlockSoundAudioComp->Play();
	}
}

void AMusicManager::OnChallengesFulfilled()
{
	bChallengesFulfilled = true;
	for (auto& Pair : AudioComponents)
	{
		Pair.Value->Stop();
	}

	UnlockSoundAudioComp->SetSound(PartySound);
	UnlockSoundAudioComp->Play();
}

void AMusicManager::OnBeat(EInstrumentType InInstrumentType, const FMusicalNote& InNextNote)
{
	if (UnlockedMusicLayers.Find(InInstrumentType) != INDEX_NONE && !bChallengesFulfilled)
	{
		ensure(AudioComponents.Find(InInstrumentType) != nullptr);
		UAudioComponent* AudioComponent = *AudioComponents.Find(InInstrumentType);
		AudioComponent->Play();
	}
}

