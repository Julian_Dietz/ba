#pragma once
#include "SoundActorInteractive.h"
#include "DispenserManager.generated.h"

UENUM()
enum class EShootMode : uint8
{
	Invalid,
	EAlternating,
	ETwoEach
};

class CShootHandler
{

public:
	CShootHandler() :
		NumActorsToChooseFrom(0),
		CurrentIndex(0),
		BeatCount(0),
		CurrentShootMode(EShootMode::Invalid)
	{
	}

	// returns index of actor that will use dispense
	int32 CheckShouldFire()
	{
		BeatCount = (BeatCount + 1) % 2;

		if (BeatCount == 0)
		{
			switch (CurrentShootMode)
			{
			case EShootMode::EAlternating:
			{
				const int32 RetVal = CurrentIndex;
				CurrentIndex = (CurrentIndex + 1) % NumActorsToChooseFrom;
				return RetVal;
			}
			case EShootMode::ETwoEach:
			{
				const int32 RetVal = CurrentIndex;
				++CurrentIndex;
				return (CurrentIndex / 2) % NumActorsToChooseFrom;
			}

			default:
				return -1;
			}
		}
		else
		{
			return -1;
		}
	}

	void SetNumActorsToChooseFrom(int32 InNumActorsToChooseFrom)
	{
		NumActorsToChooseFrom = InNumActorsToChooseFrom;
	}

	void SetShootMode(EShootMode NewMode)
	{
		CurrentShootMode = NewMode;
	}

private:
	int32 NumActorsToChooseFrom;
	int32 CurrentIndex;
	int32 BeatCount;
	EShootMode CurrentShootMode;
};


UCLASS()
class BA_PROOFCONCEPT_API ADispenserManager : public ASoundActorInteractive
{
	GENERATED_BODY()
	
public:
	ADispenserManager();
	virtual void BeginPlay() override;
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

	virtual void StartLayerChallenge() override;
	virtual void FinishLayerChallenge() override;

protected:
	UFUNCTION()
	virtual void OnBeat(EInstrumentType InInstrumentType, const FMusicalNote& InNextNote) override;

	UFUNCTION()
	void HandleCorrectInputWrapper();
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "BA|Gameplay")
	TArray<class ASoundActorDispenser*> Dispensers;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BA|Gameplay")
	EShootMode ShootMode;

private:
	int32 Index;
	int32 BeatCount;
	CShootHandler* ShootHandler;
};
