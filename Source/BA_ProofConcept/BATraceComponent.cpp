#include "BA_ProofConcept.h"
#include "BATraceComponent.h"

#include "BAFulfillComponent.h"
#include "BAPlayerController.h"
#include "BAPointableComponent.h"
#include "InputAdapterComponent.h"
#include "MotionControllerComponent.h"
#include "NoteManager.h"
#include "SoundActorInteractive.h"

UBATraceComponent::UBATraceComponent() :
	bDrawDebugVisuals(false),
	MinActivationDistance(62.0f),
	InputThreshold(0.5f),
	bHasMinDistance(false),
	CurrentInputValue(0.0f),
	bActionPressed(false),
	OnBeatVibrationIntensity(1.0f),
	OnBeatVibrationDuration(1.0f),
	TypeToVibrate(EInstrumentType::Invalid)
{
	PrimaryComponentTick.bCanEverTick = true;
}

void UBATraceComponent::BeginPlay()
{
	Super::BeginPlay();
	
	ReferenceComponent = GetOwner()->FindComponentByClass<UCameraComponent>();

	UGameplayStatics::GetAllActorsOfClass(GetWorld(), ASoundActor::StaticClass(), SoundActors);

	SetupInput();

	for (TActorIterator<ANoteManager> NoteManager(GetWorld()); NoteManager; ++NoteManager)
	{
		NoteManager->OnBeat.AddDynamic(this, &UBATraceComponent::OnBeat);
	}

	for (TActorIterator<AActor> Actor(GetWorld()); Actor; ++Actor)
	{
		if (UBAFulfillComponent* Comp = Actor->FindComponentByClass<UBAFulfillComponent>())
		{
			Comp->OnObjectiveFulfilled.AddDynamic(this, &UBATraceComponent::OnMusicLayerUnlocked);
		}
	}

	for (TActorIterator<ASoundActorInteractive> Actor(GetWorld()); Actor; ++Actor)
	{
		Actor->OnCorrectInput.AddDynamic(this, &UBATraceComponent::OnCorrectInput);
	}

	//TypeToVibrate = EInstrumentType::Kick;
}

void UBATraceComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (ReferenceComponent.IsValid())
	{
		const float CurrentDistance = (ReferenceComponent->GetComponentLocation() - GetComponentLocation()).Size();
		
		const USceneComponent* const Parent = GetAttachParent();
		const FQuat Rotation = Parent->GetComponentRotation().Quaternion();
		const FVector LineStart = Parent->GetComponentLocation() + Rotation.RotateVector(FVector::ForwardVector) * 10.0f;
		TraceEnd = LineStart + Rotation.RotateVector(FVector::ForwardVector) * 500000.0f;
		
		FHitResult HitResult;
		if(GetWorld()->LineTraceSingleByObjectType(HitResult, LineStart, TraceEnd, FCollisionObjectQueryParams(ECollisionChannel::ECC_GameTraceChannel1)))
		{
			auto* PointComp = static_cast<UBAPointableComponent*>(HitResult.Actor->GetComponentByClass(UBAPointableComponent::StaticClass()));
			if (PointComp->IsActive())
			{
				LastPointedActor = HitResult.Actor;
				PointComp->OnPointerChanged(true);
				OnTracePointerEnter.Broadcast(this, LastPointedActor.Get());
			}
			else
			{
				ResetInputActor();
			}
		}
		else if(LastPointedActor.IsValid())
		{
			auto* PointComp = static_cast<UBAPointableComponent*>(LastPointedActor->GetComponentByClass(UBAPointableComponent::StaticClass()));
			if (PointComp->IsActive())
			{
				PointComp->OnPointerChanged(false);
				OnTracePointerLeave.Broadcast(this, LastPointedActor.Get());
			}
			LastPointedActor.Reset();
			ResetInputActor();
		}

		if (GetWorld()->LineTraceSingleByChannel(HitResult, LineStart, TraceEnd, ECC_Visibility))
		{
			TraceEnd = HitResult.ImpactPoint;
		}

		if (bDrawDebugVisuals)
		{
			DrawDebugLine(GetWorld(), LineStart, TraceEnd, FColor::Red, false, -1.0f, (uint8)('\000'), 1.0f);
			GEngine->AddOnScreenDebugMessage(-1, 2.0f, FColor::Emerald, LastPointedActor.IsValid() ? LastPointedActor->GetName() : TEXT("Invalid"), true, FVector2D(3.0f, 3.0f));
		}

		if (!bHasMinDistance && CurrentDistance >= MinActivationDistance)
		{
			bHasMinDistance = true;
			OnMinDistanceChanged.Broadcast(this, true);
			
			if (const UCameraComponent* CamComp = GetOwner()->FindComponentByClass<UCameraComponent>())
			{
				const UCameraComponent* Cam = GetOwner()->FindComponentByClass<UCameraComponent>();

				const FVector Origin = Cam->GetComponentLocation();
				const FVector MyPos = Parent->GetComponentLocation();
				const FVector Dir = (MyPos - Origin).GetUnsafeNormal();
				const FVector Dir2D = FVector(0.0f, Dir.Y, Dir.Z).GetUnsafeNormal();

				const float Dot = FVector::DotProduct(Cam->GetRightVector(), Dir2D);
				const EPointDirection PointDirection = Dir.Z > 0 ? Dot > 0 ? EPointDirection::RightTop : EPointDirection::LeftTop :
					Dot > 0 ? EPointDirection::RightBottom : EPointDirection::LeftBottom;

				OnPointedSomewhere.Broadcast(this, PointDirection);

				auto PlayerController = GetWorld()->GetFirstPlayerController();
				ensure(PlayerController->GetComponentByClass(UInputAdapterComponent::StaticClass()));

				auto InputAdapter = static_cast<UInputAdapterComponent*>(PlayerController->GetComponentByClass(UInputAdapterComponent::StaticClass()));
				InputAdapter->GiveInput(EInstrumentType::DeepSynth);
			}
		}
		else if (bHasMinDistance && CurrentDistance < MinActivationDistance)
		{
			bHasMinDistance = false; 
			OnMinDistanceChanged.Broadcast(this, false);
			OnStoppedPointing.Broadcast(this);
		}
	}
}

FVector UBATraceComponent::GetTraceEnd() const
{
	return TraceEnd;
}

void UBATraceComponent::OnCorrectInput()
{
	Vibrate(OnBeatVibrationIntensity * 2.0f);
}

void UBATraceComponent::ResetInputActor()
{
	if (InputActor.IsValid())
	{
		auto* PointComp = static_cast<UBAPointableComponent*>(InputActor->GetComponentByClass(UBAPointableComponent::StaticClass()));
		PointComp->ReleaseInput();
		OnTraceInputReleased.Broadcast(this, InputActor.Get());
		InputActor.Reset();
	}
}

void UBATraceComponent::ActivateNearestSoundActor()
{
	float CurrentSmallestAngle = 360.0f;
	AActor* NearestSoundActor = nullptr;

	const FQuat Rotation = GetAttachParent()->GetComponentRotation().Quaternion();
	const FVector ForwardVec = Rotation.RotateVector(FVector::ForwardVector);

	for (int32 i = 0; i < SoundActors.Num(); ++i)
	{
		const auto& SoundActor = SoundActors[i];

		const FVector VecDirNorm = (GetComponentLocation() - SoundActor->GetActorLocation()).GetSafeNormal();
		const float CurrentAngle = FMath::Acos(FVector::DotProduct(VecDirNorm, ForwardVec));

		if (CurrentAngle < CurrentSmallestAngle)
		{
			CurrentSmallestAngle = CurrentAngle;
			NearestSoundActor = SoundActor;
		}
	}
}

void UBATraceComponent::HandleAction(float Value)
{
	// why THE FUCK!!!!!! do I have to do it like this?!?!?
	// Fuck you!
	CurrentInputValue = Value;

	if (Value > InputThreshold)
	{
		if (!bActionPressed)
		{
			bActionPressed = true;
			if (LastPointedActor.IsValid())
			{
				InputActor = LastPointedActor;
				auto* PointComp = static_cast<UBAPointableComponent*>(InputActor->GetComponentByClass(UBAPointableComponent::StaticClass()));
				PointComp->GiveInput();
				OnTraceInputGiven.Broadcast(this, InputActor.Get());
			}
		}
	}
	else
	{
		if (bActionPressed)
		{
			bActionPressed = false;
			if (InputActor.IsValid())
			{
				auto* PointComp = static_cast<UBAPointableComponent*>(InputActor->GetComponentByClass(UBAPointableComponent::StaticClass()));
				PointComp->ReleaseInput();
				OnTraceInputReleased.Broadcast(this, InputActor.Get());
				InputActor.Reset();
			}
		}
	}
}

void UBATraceComponent::OnMusicLayerUnlocked(EInstrumentType InstrumentType)
{
	//if (InstrumentType == EInstrumentType::Kick)
	//{
	//	TypeToVibrate = EInstrumentType::Kick;
	//}

	//if (InstrumentType == EInstrumentType::DeepSynth)
	//{
	//	TypeToVibrate = EInstrumentType::Snare;
	//}
}

AActor* UBATraceComponent::GetLastPointedActor() const
{
	return LastPointedActor.Get();
}

void UBATraceComponent::OnBeat(EInstrumentType InInstrumentType, const FMusicalNote& InNextNote)
{
	if (TypeToVibrate == InInstrumentType)
	{
		Vibrate(OnBeatVibrationIntensity);
	}
}

void UBATraceComponent::SetupInput()
{
	// Horrible Hack, but I'm getting annoyed of being in this room.
	if (auto MotionControllerComponent = Cast<UMotionControllerComponent>(GetAttachParent()))
	{
		auto InputComponent = GetWorld()->GetFirstPlayerController()->InputComponent;
		FName AxisName = MotionControllerComponent->Hand == EControllerHand::Left ? TEXT("ActionLeft") : TEXT("ActionRight");
		InputComponent->BindAxis(AxisName, this, &UBATraceComponent::HandleAction);
	}
}

void UBATraceComponent::Vibrate(float Intensity)
{
	APlayerController* PC = GetWorld()->GetFirstPlayerController();
	if (auto MotionControllerComponent = Cast<UMotionControllerComponent>(GetAttachParent()))
	{
		PC->PlayHapticEffect(BeatVibrationEffect, MotionControllerComponent->Hand, Intensity);
	}
}