#include "BA_ProofConcept.h"
#include "DispenserManager.h"

#include "BAFulfillComponent.h"
#include "SoundActorDispenser.h"

ADispenserManager::ADispenserManager() :
	ShootMode(EShootMode::Invalid),
	Index(0),
	BeatCount(0)
{
}

void ADispenserManager::BeginPlay()
{
	Super::BeginPlay();

	for (auto Dispenser : Dispensers)
	{
		Dispenser->OnCorrectInput.AddDynamic(this, &ADispenserManager::HandleCorrectInputWrapper);
		Dispenser->Reset();
	}

	ShootHandler = new CShootHandler();
	ShootHandler->SetNumActorsToChooseFrom(Dispensers.Num());
	ShootHandler->SetShootMode(ShootMode);
}

void ADispenserManager::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	if (ShootHandler)
	{
		delete ShootHandler;
		ShootHandler = nullptr;
	}
}

void ADispenserManager::StartLayerChallenge()
{
	Super::StartLayerChallenge();
	for (auto Dispenser : Dispensers)
	{
		Dispenser->StartLayerChallenge();
	}
}

void ADispenserManager::FinishLayerChallenge()
{
	Super::FinishLayerChallenge();
	for (auto Dispenser : Dispensers)
	{
		Dispenser->FinishLayerChallenge();
	}
}

void ADispenserManager::OnBeat(EInstrumentType InInstrumentType, const FMusicalNote& InNextNote)
{
	Super::OnBeat(InInstrumentType, InNextNote);
	
	if (InstrumentType == InInstrumentType && !bFinished)
	{
		ensure(ShootHandler);
		const int32 ShootIndex = ShootHandler->CheckShouldFire();
		if (ShootIndex != -1)
		{
			Dispensers[ShootIndex]->Dispense();
		}
/*
		BeatCount = (BeatCount + 1) % 2;
		if (BeatCount == 0)
		{
			Dispensers[Index]->Dispense();
			Index = (Index + 1) % Dispensers.Num();
		}*/
	}
}

void ADispenserManager::HandleCorrectInputWrapper()
{
	HandleCorrectInput();
}


