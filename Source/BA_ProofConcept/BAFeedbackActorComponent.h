#pragma once
#include "InstrumentType.h"
#include "Components/ActorComponent.h"
#include "BAFeedbackActorComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnSuccessfulInput, UBAFeedbackActorComponent*, InFeedbackActorComponent);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class BA_PROOFCONCEPT_API UBAFeedbackActorComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	UBAFeedbackActorComponent();
	virtual void BeginPlay() override;
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

protected:
	UFUNCTION()
	virtual void OnInputEvaluated(float TimeDifference, EInstrumentType InInstrumentType);

	UPROPERTY(BlueprintAssignable, Category = "BA")
	FOnSuccessfulInput OnSuccessfulInput;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BA|Music")
	float OnBeatThreshold;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BA|Music")
	EInstrumentType InstrumentType;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BA|Debug")
	bool bShowDebugInfo;
};
