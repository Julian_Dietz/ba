#include "BA_ProofConcept.h"
#include "BAMusicalLibrary.h"
#include "PhysicsEngine/PhysicsConstraintComponent.h"

bool BAMusicalLibrary::EvaluateInput(float& InOutTimeDifference, float InCurrentTime, FMusicalNote* PreviousNote, FMusicalNote* NextNote)
{
	ensure(PreviousNote);
	InOutTimeDifference = -1024.0f;

	if (NextNote)
	{
		if (NextNote->bHadInputYet)
		{
			return false;
		}

		InOutTimeDifference = NextNote->TimeStamp - InCurrentTime;
	}

	if (!PreviousNote->bHadInputYet)
	{
		const float PreviousTimeDiff = PreviousNote->TimeStamp - InCurrentTime;

		if (FMath::Abs(PreviousTimeDiff) < InOutTimeDifference)
		{
			InOutTimeDifference = PreviousTimeDiff;
			PreviousNote->bHadInputYet = true;
		}
		else if (NextNote)
		{
			NextNote->bHadInputYet = true;
		}
	}

	return InOutTimeDifference > -1024.0f;
}

void BAMusicalLibrary::StripPlayedNotes(TArray<FMusicalNote, TInlineAllocator<256>>& NotesToCheck, float TimeToCompareWith)
{
	while (NotesToCheck.Num() > 0 && NotesToCheck.Top().TimeStamp < TimeToCompareWith)
	{
		NotesToCheck.Pop();
	}
}


void BAMusicalLibrary::SetupConstraint(AActor* Actor, UPrimitiveComponent* Comp)
{
	//set up the constraint instance with all the desired values
	FConstraintInstance Constraint;

	Constraint.SetDisableCollision(false);
	Constraint.SetLinearXMotion(ELinearConstraintMotion::LCM_Locked);
	Constraint.SetLinearYMotion(ELinearConstraintMotion::LCM_Limited);
	Constraint.SetLinearZMotion(ELinearConstraintMotion::LCM_Locked);
	Constraint.SetLinearYLimit(ELinearConstraintMotion::LCM_Limited, 15.0f);

	Constraint.SetAngularSwing1Motion(EAngularConstraintMotion::ACM_Locked);
	Constraint.SetAngularSwing2Motion(EAngularConstraintMotion::ACM_Locked);
	Constraint.SetAngularTwistMotion(EAngularConstraintMotion::ACM_Limited);
	Constraint.SetAngularTwistLimit(EAngularConstraintMotion::ACM_Limited, 30.0f);

	Constraint.SetLinearPositionTarget(FVector(0.0f, 0.0f, 0.0f));
	Constraint.SetLinearPositionDrive(false, true, false);
	Constraint.SetLinearDriveParams(1000.0f, 0.0f, 0.0f);

	Constraint.SetAngularDriveMode(EAngularDriveMode::TwistAndSwing);
	Constraint.SetOrientationDriveTwistAndSwing(true, false);
	Constraint.SetAngularVelocityTarget(FVector::ZeroVector);
	Constraint.SetAngularDriveParams(5000.0f, 0.0f, 0.0f);

	UPhysicsConstraintComponent* ConstraintComp = NewObject<UPhysicsConstraintComponent>(Comp);
	ensure(ConstraintComp);
	ConstraintComp->RegisterComponent();

	ConstraintComp->ConstraintInstance = Constraint;
	ConstraintComp->SetWorldTransform(Comp->GetComponentTransform());
	ConstraintComp->AttachToComponent(Actor->GetRootComponent(), FAttachmentTransformRules::KeepWorldTransform);
	ConstraintComp->ConstraintActor1 = Actor;
	ConstraintComp->ConstraintActor2 = Actor;
	ConstraintComp->SetConstrainedComponents(Actor->GetRootPrimitiveComponent(), NAME_None, Comp, NAME_None);
	//ConstraintComp->ConstraintInstance.UpdateAngularLimit();
	//ConstraintComp->ConstraintInstance.UpdateLinearLimit();
}

void BAMusicalLibrary::FadeInMesh(UStaticMeshComponent*& MeshToFade, const float DeltaTime)
{
	const float StartEmissiveStrength = 100.0f;
	const float GoalOpacity = 5.0f;
	const float LerpSpeedOpacity = GoalOpacity * 2;
	const float LerpSpeedEmissive = StartEmissiveStrength * 2;

	if (UMaterialInstanceDynamic* DynMat = Cast<UMaterialInstanceDynamic>(MeshToFade->GetMaterial(0)))
	{
		FName ParameterName = FName("MaskOpacity");
		float CurrVal;
		
		if (DynMat->GetScalarParameterValue(ParameterName, CurrVal))
		{
			CurrVal += LerpSpeedOpacity * DeltaTime;
			DynMat->SetScalarParameterValue(ParameterName, CurrVal);
			UE_LOG(LogTemp, Warning, TEXT("Mesh: %s, MaskOpacity:%f"), *MeshToFade->GetName(), CurrVal);
		}

		ParameterName = FName("EmissiveStrength");

		if (DynMat->GetScalarParameterValue(ParameterName, CurrVal))
		{
			CurrVal -= LerpSpeedEmissive * DeltaTime;
			DynMat->SetScalarParameterValue(ParameterName, CurrVal);
		}

		if (CurrVal <= 0)
		{
			MeshToFade->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Overlap);
			MeshToFade = nullptr;
		}
	}
}
