#include "BA_ProofConcept.h"
#include "BAPointableComponent.h"


UBAPointableComponent::UBAPointableComponent():
	bPointedAt(false)
{
	PrimaryComponentTick.bCanEverTick = true;
}

void UBAPointableComponent::OnPointerChanged(bool bEntered)
{
	if (bEntered)
	{
		if (!bPointedAt)
		{
			bPointedAt = true;
			OnPointerEnter.Broadcast(GetOwner());
		}
	}
	else
	{
		if (bPointedAt)
		{
			bPointedAt = false;
			OnPointerLeave.Broadcast(GetOwner());
		}
	}
}

void UBAPointableComponent::GiveInput()
{
	OnInput.Broadcast(GetOwner());
}

void UBAPointableComponent::ReleaseInput()
{
	OnInputRelease.Broadcast(GetOwner());
}