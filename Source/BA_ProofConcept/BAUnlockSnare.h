#pragma once

#include "SoundActor.h"
#include "BAUnlockSnare.generated.h"

UCLASS()
class BA_PROOFCONCEPT_API ABAUnlockSnare : public ASoundActor
{
	GENERATED_BODY()
	
public:
	ABAUnlockSnare();
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaSeconds) override;
	
protected:
	UFUNCTION()
	virtual void OnBeat(EInstrumentType InInstrumentType, const FMusicalNote& InNextNote) override;

	UFUNCTION()
	void RefreshControllerRumble();

	UFUNCTION(BlueprintCallable, Category = "BA|Gameplay")
	void StartVibrating();

	UFUNCTION(BlueprintCallable, Category = "BA|Gameplay")
	void StopVibrating();

	UFUNCTION(BlueprintCallable, Category = "BA|Gameplay")
	void FulfillObjective();

	UFUNCTION(BlueprintCallable, Category = "BA|Gameplay")
	void EnableObjective();

	UFUNCTION()
	void OnClap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION(BlueprintImplementableEvent, Category = "BA|Gameplay")
	void OnClapOnBeat();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BA|Gameplay")
	UHapticFeedbackEffect_Base* BeatVibrationEffect;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BA|Gameplay")
	float MaxDistance;

private:
	TWeakObjectPtr<class ANoteManager> NoteManager;
	TArray<class UMotionControllerComponent*> MotionControllerComps;
	FTimerHandle RefreshVibrateTimerHandle;
	FMusicalNote PreviousNote, NextNote;
	float CurrentDistance;
	bool bClapped;
	bool bActiveObjective;
};
