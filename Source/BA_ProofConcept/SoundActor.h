#pragma once
#include "FMusicalNote.h"
#include "InstrumentType.h"
#include "NoteManager.h"
#include "GameFramework/Actor.h"
#include "SoundActor.generated.h"

UCLASS()
class BA_PROOFCONCEPT_API ASoundActor : public AActor
{
	GENERATED_BODY()
	
public:	
	ASoundActor();
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaSeconds) override;

	FORCEINLINE EInstrumentType GetInstrumentType() const
	{
		return InstrumentType;
	}

	FORCEINLINE void SetInstrumentType(EInstrumentType NewInstrumentType) 
	{
		InstrumentType = NewInstrumentType;
	}

protected:
	UFUNCTION()
	virtual void OnBeat(EInstrumentType InInstrumentType, const FMusicalNote& InNextNote);

	UFUNCTION(BlueprintImplementableEvent, Category = "Gameplay")
	void BP_OnBeat();

	UFUNCTION()
	virtual void OnNoteStateChanged(EInstrumentType InInstrumentType, int32 NoteThatChangedIndex, ENoteState NewNoteState);
	
	void PrintEnumDebug(const TCHAR* EnumName, uint8 Value, int32 NumPrints = 3)
	{
		for (int32 i = 0; i < NumPrints; ++i)
		{
			UEnum* pEnum = FindObject<UEnum>(ANY_PACKAGE, EnumName, true);
			FString Text = pEnum->GetNameByIndex(Value).ToString();
			GEngine->AddOnScreenDebugMessage(-1, 3.0f, FColor::Green, Text, true, FVector2D(2.0f, 2.0f));
		}
	}

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SoundLayer")
	EInstrumentType InstrumentType;

	ENoteState CurrentNoteState;
	ENoteState NoteStateOnInput;

	ANoteManager* NoteManager;
};
