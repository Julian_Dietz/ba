// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Engine/DataAsset.h"
#include "MusicDataAsset.generated.h"

/**
 * 
 */
UCLASS()
class BA_PROOFCONCEPT_API UMusicDataAsset : public UDataAsset
{
	GENERATED_BODY()

public:
	float GetBPM() const;
	
private:		
	UPROPERTY(EditAnywhere, Category = "Music")
	float BPM;
};
