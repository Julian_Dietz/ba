#pragma once
#include "FMusicalNote.h"
#include "InstrumentType.h"
#include "GameFramework/Actor.h"
#include "NoteManager.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnPreBeat, EInstrumentType, InInstrumentType, const FMusicalNote&, UpcomingNote);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnBeat, EInstrumentType, InInstrumentType, const FMusicalNote&, InNextNote);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnNoteLayerUnlocked, EInstrumentType, InstrumentType);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnLoop);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnLastNotePlayed);

DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FOnNoteStateChanged, EInstrumentType, InInstrumentType, int32, NoteThatChangedIndex, ENoteState, NewNoteState);


UCLASS()
class BA_PROOFCONCEPT_API ANoteManager : public AActor
{
	GENERATED_BODY()
	
public:	
	ANoteManager();
	virtual void PreInitializeComponents() override;
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaTime) override;

	FMusicalNote GetNextNote(EInstrumentType InstrumentType) const;
	FMusicalNote* TryGetActiveNoteByIndex(EInstrumentType InInstrumentType, int32 InNoteIndex);

	FOnPreBeat OnPreBeat;
	FOnBeat OnBeat;
	FOnLoop OnLoop;
	FOnLastNotePlayed OnLastNotePlayed;
	FOnNoteStateChanged OnNoteStateChanged;
	TMap<EInstrumentType, TArray<FMusicalNote, TInlineAllocator<256>>> MusicLayers;
	TArray<TMap<EInstrumentType, TArray<FMusicalNote, TInlineAllocator<256>>>> ActiveMusicLayersBuffer;
	UAudioComponent* AudioComp;

protected:

	UFUNCTION()
	void OnNotesStarted();

	UFUNCTION()
	void OnMusicLayerUnlocked(EInstrumentType InstrumentType);

	UPROPERTY(BlueprintAssignable, Category = "Gameplay")
	FOnNoteLayerUnlocked OnNoteLayerUnlocked;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BA|Files")
	TMap<EInstrumentType, FString> NoteFileNames;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "BA|Music")
	float BPM;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "BA|Music")
	bool bLoop;

private:
	void UpdateNotes(float TimeToUpdateWith, TMap<EInstrumentType, TArray<FMusicalNote, TInlineAllocator<256>>>& NotesToUpdate);
	void UpdateNoteLayer(TArray<FMusicalNote, TInlineAllocator<256>>& Layer, float TimeToUpdateWith);
	void ResetAllNotes();
	void EnqueueNoteBuffer(TMap<EInstrumentType, TArray<FMusicalNote, TInlineAllocator<256>>>& NotesToUpdate);
	void HandleBeat(float CurrentTime);
	void CompileNotes();

	FMusicalNote PreviousNote;

	float TimeMusicLoopStarted;
	int32 EnqueueCount;
};
