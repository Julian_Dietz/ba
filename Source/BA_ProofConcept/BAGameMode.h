#pragma once
#include "GameFramework/GameModeBase.h"
#include "BAGameMode.generated.h"

UCLASS()
class BA_PROOFCONCEPT_API ABAGameMode : public AGameModeBase
{
	GENERATED_BODY()
	
public:
	virtual void InitGame(const FString& MapName, const FString& Options, FString& ErrorMessage);
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Classes)
	TSubclassOf<APawn> VRPawnClass;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Classes)
	TSubclassOf<APawn> NonVRPawnClass;
};
