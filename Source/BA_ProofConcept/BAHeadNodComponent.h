#pragma once
#include "FMusicalNote.h"
#include "InstrumentType.h"

#include "Components/SceneComponent.h"
#include "BAHeadNodComponent.generated.h"

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class BA_PROOFCONCEPT_API UBAHeadNodComponent : public USceneComponent
{
	GENERATED_BODY()

public:	
	UBAHeadNodComponent();
	virtual void BeginPlay() override;
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

protected:
	UFUNCTION()
	void OnBeat(EInstrumentType InInstrumentType, const FMusicalNote& InNextNote);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Custom")
	EInstrumentType	InstrumentType;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Gesture|Headnod")
	float MinimumNodAmplitude;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Gesture|Headnod")
	float MaximumZOffsetOnBeat;


private:
	bool bTrackHeadMotion;
	float MaxZOffset;
	float CurrentZOffset;
	float CurrentOrientation;
	UCameraComponent* ComponentToTrack;
};
