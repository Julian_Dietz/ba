#pragma once
#include "SoundActorInteractive.h"
#include "InteractiveSoundActorManager.generated.h"

UCLASS()
class BA_PROOFCONCEPT_API AInteractiveSoundActorManager : public ASoundActorInteractive
{
	GENERATED_BODY()
	
public:
	AInteractiveSoundActorManager();
	virtual void Tick(float DeltaTime) override;

	virtual void StartLayerChallenge() override;
	virtual void FinishLayerChallenge() override;

protected:
	UFUNCTION()
	virtual void OnNoteStateChanged(EInstrumentType InInstrumentType, int32 NoteThatChangedIndex, ENoteState NewNoteState);

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "BA|Gameplay")
	TArray<class ASoundActorSingleOverlapSupp*> Actors12;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "BA|Gameplay")
	TArray<class ASoundActorSingleOverlapSupp*> Actors3;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "BA|Gameplay")
	TArray<ATargetPoint*> TargetPointsBeat1;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "BA|Gameplay")
	TArray<ATargetPoint*> TargetPointsBeat2;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "BA|Gameplay")
	TArray<ATargetPoint*> TargetPointsBeat3;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "BA|Gameplay")
	TArray<ATargetPoint*> TargetPointsBeat4;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "BA|Gameplay")
	class UBAFulfillComponent *FulfillComp0;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "BA|Gameplay")
	class UBAFulfillComponent *FulfillComp1;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "BA|Gameplay")
	int32 NumTPArraysToUse;

private: 
	void StartFadingDelayed();
	void SetUpReorderingOfActors();
	void TryReorderActors(const float DeltaTime);
	TArray<ATargetPoint*>& GetCurrentTargetPoints();

	TMap<class ASoundActorSingleOverlapSupp*, ATargetPoint*> LerpActors;

	TArray<class ASoundActorSingleOverlapSupp*>* ActiveActors;

	
	bool bHadInput;
	int32 CountCurrentBeatEnding;
};
