#pragma once
#include "InstrumentType.h"
#include "GameFramework/Actor.h"
#include "GameFlowManager.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnGameStarted);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnNotesStarted);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnChallengesFulfilled);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnGameEnded);

UCLASS()
class BA_PROOFCONCEPT_API AGameFlowManager : public AActor
{
	GENERATED_BODY()
	
public:	
	AGameFlowManager();
	virtual void BeginPlay() override;

	FOnGameStarted OnGameStarted;
	FOnNotesStarted OnNotesStarted;
	UPROPERTY(BlueprintAssignable, Category="Gameplay")
	FOnChallengesFulfilled OnChallengesFulfilled;
	FOnGameEnded OnGameEnded;

protected:
	UFUNCTION()
	void OnGaveEnoughInputsOnSoundActor(AActor* SoundActor);

	UFUNCTION(BlueprintCallable, Category = "BA|Gameflow")
	void StartNotesDelayed();

	UFUNCTION()
	void StartNotes();

	UFUNCTION()
	void EndGame();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BA|Gameflow")
	float DelayUntilMusic;

	// Because "nested containers are not supported", unreal engine punk motherfucker
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="BA|Gameflow")
	TMap<int32, class ASoundActorInteractive*> SoundActors;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BA|Gameflow")
	int32 DebugStartIndex;

private:
	int32 Index;
	FTimerHandle StartMusicTimerHandle;
};
