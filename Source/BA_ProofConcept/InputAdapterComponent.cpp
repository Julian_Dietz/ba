#include "BA_ProofConcept.h"
#include "InputAdapterComponent.h"

UInputAdapterComponent::UInputAdapterComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
}

void UInputAdapterComponent::BeginPlay()
{
	Super::BeginPlay();
}

void UInputAdapterComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}

void UInputAdapterComponent::GiveInput(EInstrumentType InInstrumentType) const
{
	OnInputGiven.Broadcast(InInstrumentType);
}

