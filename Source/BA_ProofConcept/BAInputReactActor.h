#pragma once
#include "InstrumentType.h"
#include "GameFramework/Actor.h"
#include "BAInputReactActor.generated.h"

UCLASS()
class BA_PROOFCONCEPT_API ABAInputReactActor : public AActor
{
	GENERATED_BODY()
	
public:	
	ABAInputReactActor();
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
	virtual void OnInputEvaluated(float TimeDifference, EInstrumentType InInstrumentType);
};
