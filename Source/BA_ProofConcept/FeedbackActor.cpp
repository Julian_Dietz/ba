#include "BA_ProofConcept.h"
#include "FeedbackActor.h"

#include "NoteManager.h"

AFeedbackActor::AFeedbackActor()
{
	PrimaryActorTick.bCanEverTick = true;
}

void AFeedbackActor::OnInputEvaluated(float TimeDifference, EInstrumentType InInstrumentType)
{
	// TimeDiff positive => too early
	// TimeDiff negative => too late
	FColor Color = FColor::Green;

	switch (InInstrumentType)
	{
	case EInstrumentType::Kick:
		Color = FColor::Red;
		break;
	case EInstrumentType::Snare:
		Color = FColor::Blue;
		break;
	default:
		break;
	}
}
