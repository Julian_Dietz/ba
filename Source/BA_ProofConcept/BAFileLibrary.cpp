// Fill out your copyright notice in the Description page of Project Settings.
#include "BA_ProofConcept.h"
#include "BAFileLibrary.h"

#include "Algo/Reverse.h"

/*
	File encoding rules
	
	line 1
	- Specify the time grid => add one of the following to the first line
	1 = Full Bar
	2 = Halves
	3 = Half Triples
	4 = 4th
	8 = 8ths
	12 = 8ths Triplets
	16 = 16ths
	24 = 16th Triplets
	32 = 32th

	line 2 - n
	- grid times for triggering the sound
	- num characters per line must match with specified grid space above
	- x = beat, . = break
	- spaces and | get filtered out 
	- TODO: Add pitch and velocity

	Trap Hihat example

	32
	x... x... xxxx xxxx x... x... x.x. x.x.
	xxxx xxxx xxxx x.x. x.x. x..x x..x x.xx
*/

namespace
{
	const TCHAR BeatNote = 'x';
}


float BAFileLibrary::TimeUntilLoop;


TArray<FMusicalNote, TInlineAllocator<256>> BAFileLibrary::GetCompiledNotesAsQueue(const FString& FileName, float InBPM)
{
	//InstallDir/WindowsNoEditor/GameName
	const FString FilePath = FPaths::ConvertRelativePathToFull(FPaths::GameContentDir()) + 
		+ TEXT("InstrumentLayers//") + FileName;

	ensure(FPlatformFileManager::Get().GetPlatformFile().FileExists(*FilePath));
	FString OutString, FirstLine, Rest;
	FFileHelper::LoadFileToString(OutString, FilePath.GetCharArray().GetData());
	
	TArray<FMusicalNote, TInlineAllocator<256>> NoteArray;
	int32 NumCharsFirstLine = 0;
	OutString.FindChar('\n', NumCharsFirstLine);
	FirstLine = FString(NumCharsFirstLine, OutString.GetCharArray().GetData());

	const EGridSize CurrentGridSize = static_cast<EGridSize>(FCString::Atoi(FirstLine.GetCharArray().GetData()));
	const float CurrentTimeFactor = GetStrideBetweenBeats(CurrentGridSize, InBPM);

	Rest = OutString.Mid(NumCharsFirstLine);
	Rest.ReplaceInline(L"\r", L"");
	Rest.ReplaceInline(L"\n", L"");
	Rest.ReplaceInline(L" ", L"");

	for (int32 i = 0, Index = 0; i < Rest.Len(); ++i)
	{
		if (Rest[i] == BeatNote)
		{
			NoteArray.Add(FMusicalNote(i * CurrentTimeFactor, 1.0f, 1.0f, false, Index));
			++Index;
		}
	}

	TimeUntilLoop = Rest.Len() * CurrentTimeFactor;

	// Make it a queue
	Algo::Reverse(NoteArray);

	return NoteArray;
}

float BAFileLibrary::GetStrideBetweenBeats(EGridSize InGridSize, float InBPM)
{
	float Factor = -1.0f;
	switch (InGridSize)
	{
	case EGridSize::Bar:
		Factor = 0.03125f;
		break;
	case EGridSize::HalfBar:
		Factor = 0.0625f;
		break;
	case EGridSize::HalfTriplet:
		Factor = -1.0f;
		break;
	case EGridSize::Quarter:
		Factor = 0.125f;
		break;
	case EGridSize::QuarterTriplet:
		Factor = -1.0f;
		break;
	case EGridSize::Eighth:
		Factor = 0.25f;
		break;
	case EGridSize::EightsTriplet:
		Factor = -1.0f;
		break;
	case EGridSize::SixTeenth:
		Factor = 0.5f;
		break;
	case EGridSize::SixTeenthTriplet:
		Factor = -1.0f;
		break;
	case EGridSize::ThirtyTwoth:
		Factor = 1.0f;
		break;
	}

	// 60 sec / BPM = absolute time for quarter note
	// time precision is 32th, so divide the value by 8 (1/4 => 1/32)
	// adjust the value depending on the notation grid size
	return 60.0f / (InBPM * 8.0f * Factor);
}

float BAFileLibrary::GetTimeUntilLoop()
{
	return TimeUntilLoop;
}