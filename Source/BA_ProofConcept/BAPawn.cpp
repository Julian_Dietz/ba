#include "BA_ProofConcept.h"
#include "BAPawn.h"

#include "GameFlowManager.h"
#include "IHeadMountedDisplay.h"
#include "MotionControllerComponent.h"
#include "SteamVRChaperoneComponent.h"


ABAPawn::ABAPawn():
MyRootComponent(CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"))),
CapsuleComponent(CreateDefaultSubobject<UCapsuleComponent>(TEXT("CapsuleComponent"))),
CameraComponent(CreateDefaultSubobject<UCameraComponent>(TEXT("CameraComponent"))),
SceneComponent(CreateDefaultSubobject<USceneComponent>(TEXT("SceneComponent"))),
MCComponentLeft(CreateDefaultSubobject<UMotionControllerComponent>(TEXT("MCComponentLeft"))),
MCComponentRight(CreateDefaultSubobject<UMotionControllerComponent>(TEXT("MCComponentRight"))),
SteamVRChaperoneComponent(CreateDefaultSubobject<USteamVRChaperoneComponent>(TEXT("SteamVRChaperoneComponent")))
{
	PrimaryActorTick.bCanEverTick = true;

	RootComponent = MyRootComponent;
	CameraComponent->SetupAttachment(SceneComponent);
	MCComponentLeft->SetupAttachment(SceneComponent);
	MCComponentRight->SetupAttachment(SceneComponent);
}

void ABAPawn::BeginPlay()
{
	Super::BeginPlay();

	for (TActorIterator<AGameFlowManager> GameFlowManager(GetWorld()); GameFlowManager; ++GameFlowManager)
	{
		GameFlowManager->OnGameEnded.AddDynamic(this, &ABAPawn::OnGameEnded);
	}
}

void ABAPawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ABAPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
}

void ABAPawn::OnGameEnded()
{
	BP_OnGameEnded();
}
