#include "BA_ProofConcept.h"
#include "BAInputReactActor.h"

#include "NoteManager.h"

ABAInputReactActor::ABAInputReactActor()
{
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;
}

void ABAInputReactActor::BeginPlay()
{
	Super::BeginPlay();
}

void ABAInputReactActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ABAInputReactActor::OnInputEvaluated(float TimeDifference, EInstrumentType InInstrumentType)
{
	// WILL CURRENTLY NEVER GET CALLED
}
