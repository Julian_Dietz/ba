#include "BA_ProofConcept.h"
#include "SoundActor.h"

ASoundActor::ASoundActor():
	CurrentNoteState(ENoteState::Invalid),
	NoteStateOnInput(ENoteState::Invalid),
	NoteManager(nullptr)
{
	PrimaryActorTick.bCanEverTick = true;
}

void ASoundActor::BeginPlay()
{
	Super::BeginPlay();
	for (TActorIterator<ANoteManager> ItNoteManager(GetWorld()); ItNoteManager; ++ItNoteManager)
	{
		NoteManager = *ItNoteManager;
		NoteManager->OnBeat.AddDynamic(this, &ASoundActor::OnBeat);
		NoteManager->OnNoteStateChanged.AddDynamic(this, &ASoundActor::OnNoteStateChanged);
	}
}

void ASoundActor::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
}

void ASoundActor::OnBeat(EInstrumentType InInstrumentType, const FMusicalNote& InNextNote)
{
	if (InstrumentType == InInstrumentType)
	{
		BP_OnBeat();
	}
}

void ASoundActor::OnNoteStateChanged(EInstrumentType InInstrumentType, int32 NoteThatChangedIndex, ENoteState NewNoteState)
{
}
