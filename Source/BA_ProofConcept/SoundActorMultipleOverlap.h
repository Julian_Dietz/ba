#pragma once
#include "SoundActorInteractive.h"
#include "SoundActorMultipleOverlap.generated.h"

UCLASS()
class BA_PROOFCONCEPT_API ASoundActorMultipleOverlap : public ASoundActorInteractive
{
	GENERATED_BODY()
	
public:
	ASoundActorMultipleOverlap();
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaSeconds) override;

	virtual void StartLayerChallenge() override;
	virtual void FinishLayerChallenge() override;

protected:
	UFUNCTION()
	virtual void OnBeat(EInstrumentType InInstrumentType, const FMusicalNote& InNextNote) override;

	UFUNCTION()
	virtual void OnNoteStateChanged(EInstrumentType InInstrumentType, int32 NoteThatChangedIndex, ENoteState NewNoteState) override;

	UFUNCTION()
	void OnComponentBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);


	UFUNCTION(BlueprintImplementableEvent, Category = "BA|Gameplay")
	void OnPlayerInput(UStaticMeshComponent* Comp, ENoteState NoteState);

	UFUNCTION(BlueprintImplementableEvent, Category = "BA|Gameplay")
	void BP_OnBeat_WithComp(UStaticMeshComponent* Comp);

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "BA|Gameplay")
	TMap<int32, USoundBase*> Sounds;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "BA|Gameplay")
	TMap<int32, UStaticMeshComponent*> NoteMeshPairs;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BA|Gameplay")
	class UBAFulfillComponent *FulfillSound;

private:
	void TrySwitchFadeInMesh();
	void TryFadeInMesh(const float DeltaTime);

	TMultiMap<UStaticMeshComponent*, int32> ActiveMeshes;
	int32 Index;
	UAudioComponent* Audio;
	UStaticMeshComponent* FadeInMesh;

	int32 FadeMeshIndex;
};
