#include "BA_ProofConcept.h"
#include "SoundActorDispenser.h"

#include "BAFulfillComponent.h"
#include "SoundActorInteractive.h"

ASoundActorDispenser::ASoundActorDispenser() :
	SpawnSocket(CreateDefaultSubobject<USceneComponent>(TEXT("SpawnSocket"))),
	bDispenseReady(false)
{
	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
}

void ASoundActorDispenser::BeginPlay()
{
	Super::BeginPlay();
	FulfillCompSelf->SetInstrumentType(EInstrumentType::Invalid);
}

void ASoundActorDispenser::Dispense()
{
	if (bDispenseReady)
	{
		ASoundActorInteractive* SpawnedActor = GetWorld()->SpawnActorDeferred<ASoundActorInteractive>(DispenseActorType.Get(), FTransform());
		SpawnedActor->SetInstrumentType(InstrumentType);
		SpawnedActor->SetStartedChallenge(true);
		SpawnedActor->FinishSpawning(SpawnSocket->GetComponentTransform());
		SpawnedActor->OnCorrectInput.AddDynamic(this, &ASoundActorDispenser::HandleCorrectInputWrapper);
		BP_OnDispense();
	}
}

void ASoundActorDispenser::HandleCorrectInputWrapper()
{
	HandleCorrectInput();
}
