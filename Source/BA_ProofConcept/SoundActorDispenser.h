#pragma once
#include "SoundActorInteractive.h"
#include "SoundActorDispenser.generated.h"

UCLASS()
class BA_PROOFCONCEPT_API ASoundActorDispenser : public ASoundActorInteractive
{
	GENERATED_BODY()

public:
	ASoundActorDispenser();
	virtual void BeginPlay() override;

	void Dispense();

protected:
	UFUNCTION(BlueprintImplementableEvent)
	void BP_OnDispense();

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="BA|Gameplay")
	TSubclassOf<class ASoundActorInteractive> DispenseActorType;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BA|Gameplay")
	USceneComponent* SpawnSocket;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "BA|Gameplay")
	bool bDispenseReady;

private:
	
	UFUNCTION()
	void HandleCorrectInputWrapper();
};

