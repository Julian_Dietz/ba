#include "BA_ProofConcept.h"
#include "BAFulfillComponent.h"

UBAFulfillComponent::UBAFulfillComponent():
	InstrumentType(EInstrumentType::Invalid),
	bNoteUnlocked(false),
	bSoundUnlocked(false)
{
	PrimaryComponentTick.bCanEverTick = true;
}

void UBAFulfillComponent::SetFulfilled()
{
	if (!bNoteUnlocked && InstrumentType != EInstrumentType::Invalid)
	{
		bNoteUnlocked = true;
		OnObjectiveFulfilled.Broadcast(InstrumentType);
	}
}

void UBAFulfillComponent::UnlockSoundLayer()
{
	if (!bSoundUnlocked && InstrumentType != EInstrumentType::Invalid)
	{
		bSoundUnlocked = true;
		OnSoundUnlocked.Broadcast(InstrumentType);
	}
}

void UBAFulfillComponent::Reset()
{
	//TODO: Implement Reset function - if needed
	ensure(false);
}
