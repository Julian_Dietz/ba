#include "BA_ProofConcept.h"
#include "BAGameFlowActor.h"

#include "GameFlowManager.h"

ABAGameFlowActor::ABAGameFlowActor()
{
	PrimaryActorTick.bCanEverTick = false;
}

void ABAGameFlowActor::BeginPlay()
{
	Super::BeginPlay();
	for (TActorIterator<AGameFlowManager> GameFlowManager(GetWorld()); GameFlowManager; ++GameFlowManager)
	{
		GameFlowManager->OnGameStarted.AddDynamic(this, &ABAGameFlowActor::OnGameStarted);
		GameFlowManager->OnGameEnded.AddDynamic(this, &ABAGameFlowActor::OnGameEnded);
	}
}

void ABAGameFlowActor::OnGameStarted()
{
	BP_OnGameStarted();
}

void ABAGameFlowActor::OnGameEnded() 
{
	BP_OnGameEnded();
}
