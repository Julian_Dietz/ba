#pragma once
#include "Components/ActorComponent.h"
#include "BAPointableComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnInput, AActor*, Owner);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnInputRelease, AActor*, Owner);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnPointerEnter, AActor*, Owner);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnPointerLeave, AActor*, Owner);

UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class BA_PROOFCONCEPT_API UBAPointableComponent : public UActorComponent
{
	GENERATED_BODY()
	
public:	
	UBAPointableComponent();

	void OnPointerChanged(bool bEntered);
	void GiveInput();
	void ReleaseInput();

protected:
	UPROPERTY(BlueprintAssignable, Category = "Input")
	FOnInput OnInput;

	UPROPERTY(BlueprintAssignable, Category = "Input")
	FOnInput OnInputRelease;

	UPROPERTY(BlueprintAssignable, Category="Input")
	FOnPointerEnter OnPointerEnter;

	UPROPERTY(BlueprintAssignable, Category = "Input")
	FOnPointerLeave OnPointerLeave;

private:
	bool bPointedAt;
};
