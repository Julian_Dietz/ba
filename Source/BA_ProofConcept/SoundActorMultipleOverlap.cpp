#include "BA_ProofConcept.h"
#include "SoundActorMultipleOverlap.h"
#include "PhysicsEngine/PhysicsConstraintComponent.h"

#include "BAFulfillComponent.h"
#include "BAMusicalLibrary.h"
#include "BAPawn.h"

ASoundActorMultipleOverlap::ASoundActorMultipleOverlap() :
	FulfillSound(CreateDefaultSubobject<UBAFulfillComponent>(TEXT("FulfillSound"))),
	Index(0),
	Audio(CreateDefaultSubobject<UAudioComponent>(TEXT("AudioComponent"))),
	FadeInMesh(nullptr),
	FadeMeshIndex(0)
{
}

void ASoundActorMultipleOverlap::BeginPlay()
{
	Super::BeginPlay();

	TArray<UStaticMeshComponent*> MeshComps;
	GetComponents(MeshComps);
	for (auto Mesh : MeshComps)
	{
		Mesh->SetSimulatePhysics(true);
		Mesh->SetEnableGravity(false);
		Mesh->bGenerateOverlapEvents = true;
		Mesh->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);

		UMaterialInstanceDynamic* DynMat = Mesh->CreateDynamicMaterialInstance(0, CubeMaterial ? 
			CubeMaterial : Mesh->GetMaterial(0));
		Mesh->SetMaterial(0, DynMat);

		BAMusicalLibrary::SetupConstraint(this, Mesh);
		
		Mesh->OnComponentBeginOverlap.AddDynamic(this, &ASoundActorMultipleOverlap::OnComponentBeginOverlap);
	}
}

void ASoundActorMultipleOverlap::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
	if (!bFinished)
	{
		if (FadeMeshIndex >= -1 && FadeInMesh)
		{
			TryFadeInMesh(DeltaSeconds);
		}
		else
		{
			for (auto& ActiveMeshPair : ActiveMeshes)
			{
				UStaticMeshComponent* ActiveMesh = ActiveMeshPair.Key;
				TArray<FOverlapInfo> Overlaps = ActiveMesh->GetOverlapInfos();

				for (int32 i = 0; i < Overlaps.Num(); ++i)
				{
					const FOverlapInfo Info = Overlaps[i];
					if (Info.OverlapInfo.Actor->IsA<ABAPawn>())
					{
						if (FMusicalNote* CurrentNote = NoteManager->TryGetActiveNoteByIndex(InstrumentType, ActiveMeshPair.Value))
						{
							const ENoteState NoteState = CurrentNote->GetCurrentNoteState(); /*ActiveMeshPair.Value->GetCurrentNoteState();*/

							if (NoteState > ENoteState::NotStarted &&  NoteState < ENoteState::Ended &&
								!CurrentNote->bHadInputYet)
							{
								CurrentNote->bHadInputYet = true;
								OnPlayerInput(ActiveMesh, NoteState);

								ActiveMeshes.RemoveSingle(ActiveMeshPair.Key, ActiveMeshPair.Value);
								HandleCorrectInput();
							}
							break;
						}
					}
				}
			}
		}
	}
}

void ASoundActorMultipleOverlap::StartLayerChallenge()
{
	FulfillCompSelf->SetFulfilled();
	FulfillSound->SetFulfilled();
	FulfillSound->UnlockSoundLayer();
	BP_StartLayerChallenge();

	TArray<UStaticMeshComponent*> MeshComps;
	GetComponents(MeshComps);

	FadeMeshIndex = MeshComps.Num() - 1;
	TrySwitchFadeInMesh();
}

void ASoundActorMultipleOverlap::FinishLayerChallenge()
{
	if (bPrintDebugInfo)
	{
		GEngine->AddOnScreenDebugMessage(-1, 5.0f, FColor::Cyan, TEXT(__FUNCTION__), true, FVector2D(3.0f, 3.0f));
	}

	BP_FinishLayerChallenge();
}

void ASoundActorMultipleOverlap::OnBeat(EInstrumentType InInstrumentType, const FMusicalNote& InNextNote)
{
	Super::OnBeat(InInstrumentType, InNextNote);
	if (InInstrumentType == InstrumentType && !bFinished && FadeMeshIndex < 0 && !FadeInMesh)
	{
		if (auto Mesh = NoteMeshPairs.Find(InNextNote.NoteIndex))
		{
			BP_OnBeat_WithComp(*Mesh);
		}
	}
}

void ASoundActorMultipleOverlap::OnNoteStateChanged(EInstrumentType InInstrumentType, int32 NoteThatChangedIndex, ENoteState NewNoteState)
{
	Super::OnNoteStateChanged(InInstrumentType, NoteThatChangedIndex, NewNoteState);

	if (InInstrumentType == InstrumentType)
	{
		if (NewNoteState == ENoteState::NotStarted)
		{
			if (auto Mesh = NoteMeshPairs.Find(NoteThatChangedIndex))
			{
				ActiveMeshes.Add(*Mesh, NoteThatChangedIndex);
			}
		}
		else if (NewNoteState == ENoteState::Ended)
		{
			if (auto Mesh = NoteMeshPairs.Find(NoteThatChangedIndex))
			{
				ActiveMeshes.RemoveSingle(*Mesh, NoteThatChangedIndex);
			}
		}
	}
}

void ASoundActorMultipleOverlap::OnComponentBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherActor->IsA<ABAPawn>())
	{
		const FVector ImpulseDirection = (OverlappedComp->GetComponentLocation() - OtherComp->GetComponentLocation()).GetUnsafeNormal();
		const float ImpulseStrength = 250.0f;

		OverlappedComp->AddImpulse(ImpulseDirection * ImpulseStrength, NAME_None, true);
	}
}

void ASoundActorMultipleOverlap::TrySwitchFadeInMesh()
{
	if (FadeMeshIndex >= 0)
	{
		TArray<UStaticMeshComponent*> MeshComps;
		GetComponents(MeshComps);
		FadeInMesh = MeshComps[FadeMeshIndex--];
	}
}

void ASoundActorMultipleOverlap::TryFadeInMesh(const float DeltaTime)
{
	if (FadeInMesh)
	{
		BAMusicalLibrary::FadeInMesh(FadeInMesh, DeltaTime);
		if (!FadeInMesh)
		{
			TrySwitchFadeInMesh();
		}
	}
}

