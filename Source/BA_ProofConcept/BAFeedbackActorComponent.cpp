#include "BA_ProofConcept.h"
#include "BAFeedbackActorComponent.h"

#include "NoteManager.h"
#include "SoundActor.h"

UBAFeedbackActorComponent::UBAFeedbackActorComponent():
	OnBeatThreshold(0.15f),
	InstrumentType(EInstrumentType::Invalid),
	bShowDebugInfo(false)
{
	PrimaryComponentTick.bCanEverTick = true;
}


void UBAFeedbackActorComponent::BeginPlay()
{
	Super::BeginPlay();

	for (TActorIterator<ANoteManager> NoteManager(GetWorld()); NoteManager; ++NoteManager)
	{
		//NoteManager->OnInputEvaluated.AddDynamic(this, &UBAFeedbackActorComponent::OnInputEvaluated);
	}

	if (auto Owner = Cast<ASoundActor>(GetOwner()))
	{
		InstrumentType = Owner->GetInstrumentType();
	}
}


void UBAFeedbackActorComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}


void UBAFeedbackActorComponent::OnInputEvaluated(float TimeDifference, EInstrumentType InInstrumentType)
{
	// TimeDiff positive => too early
	// TimeDiff negative => too late

	if (bShowDebugInfo)
	{
		FColor Color = FColor::Green;

		switch (InInstrumentType)
		{
		case EInstrumentType::Kick:
			Color = FColor::Red;
			break;
		case EInstrumentType::Snare:
			Color = FColor::Blue;
			break;
		default:
			break;
		}

		for(int i= 0; i < 5; ++i)
		GEngine->AddOnScreenDebugMessage(-1, 5.0f, Color, FString::SanitizeFloat(TimeDifference));
	}

	if (InstrumentType == InInstrumentType && TimeDifference <= OnBeatThreshold)
	{
		OnSuccessfulInput.Broadcast(this);
	}
}

