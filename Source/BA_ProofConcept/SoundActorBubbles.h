#pragma once
#include "SoundActorInteractive.h"
#include "SoundActorBubbles.generated.h"

UCLASS()
class BA_PROOFCONCEPT_API ASoundActorBubbles : public ASoundActorInteractive
{
	GENERATED_BODY()
	
public:
	ASoundActorBubbles();
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaSeconds) override;

protected:
	UFUNCTION()
	virtual void OnBeat(EInstrumentType InInstrumentType, const FMusicalNote& InNextNote) override;

	UFUNCTION()
	virtual void OnNoteStateChanged(EInstrumentType InInstrumentType, int32 NoteThatChangedIndex, ENoteState NewNoteState) override;

	UFUNCTION(BlueprintImplementableEvent, Category = "BA|Gameplay")
	void OnPlayerInput(UStaticMeshComponent* Comp, ENoteState NoteState);

	UFUNCTION(BlueprintImplementableEvent, Category = "BA|Gameplay")
	void BP_OnBeat_WithComp(UStaticMeshComponent* Comp);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BA|Gameplay")
	TMap<int32, USoundBase*> Sounds;

	TMap<int32, UStaticMeshComponent*> Meshes;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	UStaticMeshComponent* Mesh0;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	UStaticMeshComponent* Mesh1;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	UStaticMeshComponent* Mesh2;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	UStaticMeshComponent* Mesh3;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	UStaticMeshComponent* Mesh4;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	UStaticMeshComponent* Mesh5;

private:
	TMultiMap<UStaticMeshComponent*, int32> ActiveMeshes;
	int32 Index;
	UAudioComponent* Audio;
};
