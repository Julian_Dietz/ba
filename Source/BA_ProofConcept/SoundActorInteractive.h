#pragma once
#include "SoundActor.h"
#include "SoundActorInteractive.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnGaveEnoughInputsOnSoundActor, AActor*, SoundActor);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnCorrectInput);

UCLASS()
class BA_PROOFCONCEPT_API ASoundActorInteractive : public ASoundActor
{
	GENERATED_BODY()

public:
	ASoundActorInteractive();
	virtual void BeginPlay() override;

	virtual void StartLayerChallenge();
	virtual void FinishLayerChallenge();

	UFUNCTION(BlueprintCallable, Category="BA|Gameplay")
	void Reset();

	UFUNCTION(BlueprintCallable, Category = "BA|Gameplay")
	void SetNumCorrectHits(int32 NewCorrectHits)
	{
		NumCorrectHits = NewCorrectHits;
	}

	UFUNCTION(BlueprintPure, Category = "BA|Gameplay")
	bool GetStartedChallenge()
	{
		return bStartedChallenge;
	}

	void SetStartedChallenge(bool InStartedChallenge)
	{
		bStartedChallenge = InStartedChallenge;
	}

	UPROPERTY(BlueprintAssignable)
	FOnGaveEnoughInputsOnSoundActor OnGaveEnoughInputsOnSoundActor;

	UPROPERTY(BlueprintAssignable)
	FOnCorrectInput OnCorrectInput;

protected:
	UFUNCTION()
	virtual void OnNoteStateChanged(EInstrumentType InInstrumentType, int32 NoteThatChangedIndex, ENoteState NewNoteState);
	UFUNCTION()
	virtual void OnBeat(EInstrumentType InInstrumentType, const FMusicalNote& InNextNote) override;
	UFUNCTION()
	void OnLoop();
	UFUNCTION(BlueprintImplementableEvent, Category = "Gameplay")
	void BP_StartLayerChallenge();
	UFUNCTION(BlueprintImplementableEvent, Category = "Gameplay")
	void BP_FinishLayerChallenge();

	void HandleCorrectInput();

	UFUNCTION(BlueprintImplementableEvent, Category = "BA|Gameplay")
	void OnInput(ENoteState NoteState);

	UFUNCTION(BlueprintPure, Category = "BA|Gameplay")
	FORCEINLINE int32 GetNumCorrectHits()
	{
		return NumCorrectHits;
	}

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "BA|Gameplay")
	bool bWaitUntilLoopForFeedback;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "BA|Gameplay")
	bool bAutoAdjustUnlockSelfComp;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "BA|Gameplay")
	bool bPrintDebugInfo;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BA|Gameplay")
	int32 NumCorrectHitsNeeded;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BA|Gameplay")
	int32 NumCorrectLoopsNeeded;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BA|Gameplay")
	class UMaterialInterface *CubeMaterial;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "BA|Gameplay")
	class UBAFulfillComponent *FulfillCompSelf;

	int32 NumCorrectHits;
	int32 NumCorrectLoops;


	bool bHadAllInputs;
	bool bStartedChallenge;
	bool bFinished;
};
