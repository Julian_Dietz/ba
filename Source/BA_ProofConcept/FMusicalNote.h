#pragma once
#include "FMusicalNote.generated.h"

UENUM(BlueprintType)
enum class ENoteState : uint8
{
	Invalid,
	NotStarted,
	PreBeat,
	OnBeat,
	PostBeat,
	Ended
};

USTRUCT()
struct FMusicalNote
{
	GENERATED_BODY()

	FMusicalNote(float InTimeStamp = 0.0f, float InVolume = 1.0f, float InPitch = 1.0f, bool InPlayed = false, int32 InNoteIndex = -1);

	FORCEINLINE ENoteState GetCurrentNoteState() const
	{
		return CurrentNoteState;
	}

	bool UpdateNoteState(const float CurrentTime);

	// The time when this note would be played. Relative to the beginning of the song.
	float TimeStamp;

	// Default value is 1.0f
	float Volume;

	// Default value is 1.0f Lower to get a deeper pitch and vice versa
	float Pitch;
	
	int32 NoteIndex;

	float PreBeat_Threshold;
	float OnBeat_Theshold;
	float PostBeat_Threshold;
	float PreAndPostTime;

	ENoteState CurrentNoteState;

public:
	// If the beat for the note has played yet
	bool bPlayedBeatYet;

	// If the note has been affected by input yet
	bool bHadInputYet;
};
