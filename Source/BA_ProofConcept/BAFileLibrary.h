// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "FMusicalNote.h"

class BA_PROOFCONCEPT_API BAFileLibrary
{
public:
	enum EGridSize
	{
		Invalid = 0,
		Bar = 1,
		HalfBar = 2,
		HalfTriplet = 3,
		Quarter = 4,
		QuarterTriplet = 6,
		Eighth = 8,
		EightsTriplet = 12,
		SixTeenth = 16,
		SixTeenthTriplet = 24,
		ThirtyTwoth = 32,
	};
	
	static TArray<FMusicalNote, TInlineAllocator<256>> GetCompiledNotesAsQueue(const FString& FileName, float InBPM);
	static float GetStrideBetweenBeats(EGridSize InGridSize, float InBPM);
	static float GetTimeUntilLoop();

private:
	static float TimeUntilLoop;
};
