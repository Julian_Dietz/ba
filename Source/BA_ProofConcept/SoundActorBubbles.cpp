#include "BA_ProofConcept.h"
#include "SoundActorBubbles.h"

#include "BAPawn.h"

ASoundActorBubbles::ASoundActorBubbles() :
	Index(0)
{
	Audio = CreateDefaultSubobject<UAudioComponent>(TEXT("AudioComponent"));
	Mesh0 = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComp_0"));
	Mesh1 = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComp_1"));
	Mesh2 = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComp_2"));
	Mesh3 = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComp_3"));
	Mesh4 = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComp_4"));
	Mesh5 = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComp_5"));

	Meshes.Add(0, Mesh0);
	Meshes.Add(1, Mesh1);
	Meshes.Add(2, Mesh0);
	Meshes.Add(3, Mesh1);
	Meshes.Add(4, Mesh2);
	Meshes.Add(5, Mesh3);
	Meshes.Add(6, Mesh4);
	Meshes.Add(7, Mesh5);
	Meshes.Add(8, Mesh4);
	Meshes.Add(9, Mesh5);

	InstrumentType = EInstrumentType::BubblesMuted;
}

void ASoundActorBubbles::BeginPlay()
{
	Super::BeginPlay();

	for (auto& Mesh : Meshes)
	{
		Mesh.Value->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Overlap);
		auto* DynMat = Mesh.Value->CreateDynamicMaterialInstance(0, Mesh.Value->GetMaterial(0));
		Mesh.Value->SetMaterial(0, DynMat);
	}
}

void ASoundActorBubbles::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
	for(auto& ActiveMeshPair : ActiveMeshes)
	{
		UStaticMeshComponent* ActiveMesh = ActiveMeshPair.Key;
		TArray<FOverlapInfo> Overlaps = ActiveMesh->GetOverlapInfos();

		for (int32 i = 0; i < Overlaps.Num(); ++i)
		{
			const FOverlapInfo Info = Overlaps[i];
			if (Info.OverlapInfo.Actor->IsA<ABAPawn>())
			{
				if (FMusicalNote* CurrentNote = NoteManager->TryGetActiveNoteByIndex(InstrumentType, ActiveMeshPair.Value))
				{
					const ENoteState NoteState = CurrentNote->GetCurrentNoteState(); /*ActiveMeshPair.Value->GetCurrentNoteState();*/

					if (NoteState > ENoteState::NotStarted &&  NoteState < ENoteState::Ended &&
						!CurrentNote->bHadInputYet)
					{
						CurrentNote->bHadInputYet = true;
						// Now we had Input and we know to which state it happened
						OnPlayerInput(ActiveMesh, NoteState);
						//PrintEnumDebug(TEXT("ENoteState"), (uint8)NoteState, 2);

						ActiveMeshes.RemoveSingle(ActiveMeshPair.Key, ActiveMeshPair.Value);
					}
					break;
				}
			}
		}
	}
}

void ASoundActorBubbles::OnBeat(EInstrumentType InInstrumentType, const FMusicalNote& InNextNote)
{
	Super::OnBeat(InInstrumentType, InNextNote);
	if (InInstrumentType == InstrumentType)
	{
		if (auto Sound = Sounds.Find(InNextNote.NoteIndex))
		{
			Audio->SetSound(*Sound);
			Audio->Play();
		}

		if (auto Mesh = Meshes.Find(InNextNote.NoteIndex))
		{
			BP_OnBeat_WithComp(*Mesh);
		}
	}
}

void ASoundActorBubbles::OnNoteStateChanged(EInstrumentType InInstrumentType, int32 NoteThatChangedIndex, ENoteState NewNoteState)
{
	Super::OnNoteStateChanged(InInstrumentType, NoteThatChangedIndex, NewNoteState);

	if (InInstrumentType == InstrumentType)
	{
		if (NewNoteState == ENoteState::NotStarted)
		{
			if (auto Mesh = Meshes.Find(NoteThatChangedIndex))
			{
				ActiveMeshes.Add(*Mesh, NoteThatChangedIndex);
			}
		}
		else if(NewNoteState == ENoteState::Ended)
		{
			if (auto Mesh = Meshes.Find(NoteThatChangedIndex))
			{
				ActiveMeshes.RemoveSingle(*Mesh, NoteThatChangedIndex);
			}
		}
	}
}