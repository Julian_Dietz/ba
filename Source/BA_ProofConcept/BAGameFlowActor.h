#pragma once

#include "GameFramework/Actor.h"
#include "BAGameFlowActor.generated.h"

UCLASS()
class BA_PROOFCONCEPT_API ABAGameFlowActor : public AActor
{
	GENERATED_BODY()
	
public:	
	ABAGameFlowActor();
	virtual void BeginPlay() override;

protected:

	UFUNCTION()
	virtual void OnGameStarted();
	UFUNCTION()
	virtual void OnGameEnded();

	UFUNCTION(BlueprintImplementableEvent, Category="BA|Gameflow")
	void BP_OnGameStarted();
	UFUNCTION(BlueprintImplementableEvent, Category = "BA|Gameflow")
	void BP_OnGameEnded();
};
