#pragma once
#include "BAInputReactActor.h"
#include "BAGrowingSplineActor.generated.h"

UCLASS()
class BA_PROOFCONCEPT_API ABAGrowingSplineActor : public ABAInputReactActor
{
	GENERATED_BODY()
	
public:	
	ABAGrowingSplineActor();
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaSeconds) override;
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

protected:
	UFUNCTION()
	virtual void OnInputEvaluated(float TimeDifference, EInstrumentType InInstrumentType) override;

	UFUNCTION()
	void GrowNewHead();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	class USplineComponent* SplineComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UStaticMesh* SplineMeshType;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UMaterialInterface* SplineMeshMaterial;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grow")
	float GrowInterval;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grow")
	float GrowSpeed;

	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grow")
	UParticleSystemComponent* Emitter;

private:
	void AddSplinePointAtEnd();
	void AddSplineMesh(int32 IndexBeginPoint, int32 IndexEndPoint);
	void UpdateSplineEnd(float DeltaSeconds);

	int32 LastSplinePointIndex;
	class USplineMeshComponent* LatestSplineMesh;
	FTimerHandle ChangeGrowTimerHandle;
	FVector GrowDirection;


};
