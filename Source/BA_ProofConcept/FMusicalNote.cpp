// Fill out your copyright notice in the Description page of Project Settings.
#include "BA_ProofConcept.h"
#include "FMusicalNote.h"

FMusicalNote::FMusicalNote(float InTimeStamp, float InVolume, float InPitch, bool bInPlayed, int32 InNoteIndex):
	TimeStamp(InTimeStamp),
	Volume(InVolume),
	Pitch(InPitch),
	NoteIndex(InNoteIndex),
	PreBeat_Threshold(0.075f),
	OnBeat_Theshold(0.25f),
	PostBeat_Threshold(0.075f),
	PreAndPostTime(0.1f),
	CurrentNoteState(ENoteState::Invalid),
	bPlayedBeatYet(false),
	bHadInputYet(bInPlayed)
{
}

bool FMusicalNote::UpdateNoteState(const float CurrentTime)
{
	const ENoteState State = CurrentNoteState;
	const float TimeOnBeat_LowerBound = TimeStamp - (OnBeat_Theshold * 0.5f);
	const float TimeOnBeat_UpperBound = TimeStamp + (OnBeat_Theshold * 0.5f);
	const float TimePreBeat = TimeOnBeat_LowerBound - PreBeat_Threshold;
	const float TimePostBeat = TimeOnBeat_UpperBound + PostBeat_Threshold;

	if (CurrentTime <= TimePreBeat - PreAndPostTime)
	{
		CurrentNoteState = ENoteState::Invalid;
	}
	else if (CurrentTime < TimePreBeat && CurrentTime > TimePreBeat - PreAndPostTime)
	{
		CurrentNoteState = ENoteState::NotStarted;
	}
	else if (CurrentTime < TimeOnBeat_LowerBound && CurrentTime >= TimePreBeat)
	{
		CurrentNoteState = ENoteState::PreBeat;
	}
	else if (CurrentTime > TimeOnBeat_LowerBound && CurrentTime < TimeOnBeat_UpperBound)
	{
		CurrentNoteState = ENoteState::OnBeat;
	}
	else if (CurrentTime > TimeOnBeat_UpperBound && CurrentTime <= TimePostBeat)
	{
		CurrentNoteState = ENoteState::PostBeat;
	}
	else if (CurrentTime > TimePostBeat && CurrentTime <= TimePostBeat + PreAndPostTime)
	{
		CurrentNoteState = ENoteState::Ended;
	}
	else if (CurrentTime > TimePostBeat + PreAndPostTime)
	{
		CurrentNoteState = ENoteState::Invalid;
	}

	return State != CurrentNoteState;
}