#pragma once
#include "Components/ActorComponent.h"
#include "BAChargeableComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnChargedQuarter, UBAChargeableComponent*, Component);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnChargedHalf, UBAChargeableComponent*, Component);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnChargedThreeQuarter, UBAChargeableComponent*, Component);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnChargedFull, UBAChargeableComponent*, Component);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class BA_PROOFCONCEPT_API UBAChargeableComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	UBAChargeableComponent();

public:	
	virtual void BeginPlay() override;
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UFUNCTION(BlueprintCallable, Category = "Charge")
	void SetCharge(bool bInCharge)
	{
		bCharge = bInCharge;
	}

	UFUNCTION(BlueprintPure, Category = "Charge")
	FORCEINLINE	bool GetCharge()
	{
		return bCharge;
	}

	UFUNCTION(BlueprintCallable, Category = "Charge")
	float GetCurrentChargePercentage() const;

protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Charge")
	float ChargeTime;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Charge")
	float DechargeTimePerSecond;

	UPROPERTY(BlueprintAssignable)
	FOnChargedQuarter OnChargedQuarter;

	UPROPERTY(BlueprintAssignable)
	FOnChargedHalf OnChargedHalf;

	UPROPERTY(BlueprintAssignable)
	FOnChargedThreeQuarter OnChargedThreeQuarter;

	UPROPERTY(BlueprintAssignable)
	FOnChargedFull OnChargedFull;

private:
	bool bCharge;
	float CurrentChargeTime;

	bool bChargedQuarter;
	bool bChargedHalf;
	bool bChargedThreeQuarter;
};
