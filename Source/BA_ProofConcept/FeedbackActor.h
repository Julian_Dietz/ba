#pragma once
#include "BAInputReactActor.h"
#include "FeedbackActor.generated.h"

UCLASS()
class BA_PROOFCONCEPT_API AFeedbackActor : public ABAInputReactActor
{
	GENERATED_BODY()
	
public:	
	AFeedbackActor();

	UFUNCTION()
	virtual void OnInputEvaluated(float TimeDifference, EInstrumentType InInstrumentType) override;
};
