#pragma once

UENUM(BlueprintType)
enum class EInstrumentType : uint8
{
	Invalid,
	Kick,
	Snare,
	Hihat,
	Cymbal,
	Bass,
	DeepSynth,
	HighSynth,
	HighSynthMuted,
	Bubbles,
	BubblesMuted,
	HighSynthExtended,
	HighSynthExtendedMuted,
	Melody,
	Clap,
	ClapAndHihat
};