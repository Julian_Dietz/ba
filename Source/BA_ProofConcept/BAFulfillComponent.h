#pragma once
#include "InstrumentType.h"
#include "Components/ActorComponent.h"
#include "BAFulfillComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnObjectiveFulfilled, EInstrumentType, InstrumentType);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnSoundUnlocked, EInstrumentType, InstrumentType);


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class BA_PROOFCONCEPT_API UBAFulfillComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	friend class AInteractiveSoundActorManager;
	friend class ASoundActorInteractive;

	UBAFulfillComponent();

	UFUNCTION(BlueprintCallable, Category = "BA|Objective")
	void SetFulfilled();

	UFUNCTION(BlueprintCallable, Category = "BA|Objective")
	void UnlockSoundLayer();

	UFUNCTION(BlueprintCallable, Category = "BA|Objective")
	void Reset();

	UPROPERTY(BlueprintAssignable, Category = "BA|Objective")
	FOnObjectiveFulfilled OnObjectiveFulfilled;

	UPROPERTY(BlueprintAssignable, Category = "BA|Objective")
	FOnSoundUnlocked OnSoundUnlocked;

	FORCEINLINE void SetInstrumentType(EInstrumentType NewInstrumentType)
	{
		InstrumentType = NewInstrumentType;
	}

	FORCEINLINE EInstrumentType GetInstrumentType() const
	{
		return InstrumentType;
	}

protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BA|Objective")
	EInstrumentType InstrumentType;
	
private:
	bool bNoteUnlocked;
	bool bSoundUnlocked;
};
