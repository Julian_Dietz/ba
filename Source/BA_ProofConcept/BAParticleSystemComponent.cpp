#include "BA_ProofConcept.h"
#include "BAParticleSystemComponent.h"

void UBAParticleSystemComponent::SetParticleVelocities(FName EmitterName, FVector NewVelocity, bool bRandomizePerParticle, float SpeedRandom)
{
	const int32 NumEmitters = EmitterInstances.Num();
	for (int32 i = 0; i < NumEmitters; ++i)
	{
		if (FParticleEmitterInstance* Instance = EmitterInstances[i])
		{
			if (EmitterName.IsNone() || Instance->SpriteTemplate->EmitterName.IsEqual(EmitterName))
			{
				SetEmitterParticleVelocities(Instance, NewVelocity, bRandomizePerParticle, SpeedRandom);
			}
		}
	}
}

void UBAParticleSystemComponent::BoostParticleVelocities(FName EmitterName, float Amount)
{
	const int32 NumEmitters = EmitterInstances.Num();
	for (int32 i = 0; i < NumEmitters; ++i)
	{
		if (FParticleEmitterInstance* Instance = EmitterInstances[i])
		{
			if (EmitterName.IsNone() || Instance->SpriteTemplate->EmitterName.IsEqual(EmitterName))
			{
				BoostEmitterParticleVelocities(Instance, Amount);
			}
		}
	}
}

void UBAParticleSystemComponent::SetEmitterParticleVelocities(FParticleEmitterInstance* EmitterInstance, FVector NewVelocity, bool bRandomizePerParticle, float SpeedRandom)
{
	const int32 NumPerRandom = 4;
	int32 CurrentRandom = 0;
	const int32 ActiveParticles = EmitterInstance->ActiveParticles;
	for (int32 ParticleIndex = 0; ParticleIndex < ActiveParticles; ParticleIndex++)
	{
		if (auto Particle = EmitterInstance->GetParticleDirect(ParticleIndex))
		{
			if (bRandomizePerParticle && CurrentRandom++ % NumPerRandom == 0)
			{
				NewVelocity = FVector(FMath::FRand() * 2.0f - 1.0f, FMath::FRand() * 2.0f - 1.0f, FMath::FRandRange(-0.2f, 1.0f)) * SpeedRandom;
			}
			Particle->BaseVelocity = NewVelocity;
		}
	}
}

void UBAParticleSystemComponent::BoostEmitterParticleVelocities(FParticleEmitterInstance* EmitterInstance, float Amount)
{
	const int32 ActiveParticles = EmitterInstance->ActiveParticles;
	for (int32 ParticleIndex = 0; ParticleIndex < ActiveParticles; ParticleIndex++)
	{
		if (auto Particle = EmitterInstance->GetParticleDirect(ParticleIndex))
		{
			Particle->BaseVelocity = Particle->BaseVelocity * Amount;
		}
	}
}