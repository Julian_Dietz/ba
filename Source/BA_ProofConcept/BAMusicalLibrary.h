// Fill out your copyright notice in the Description page of Project Settings.

#pragma once
#include "FMusicalNote.h"

class BA_PROOFCONCEPT_API BAMusicalLibrary
{
public:
	UFUNCTION(BlueprintCallabe, Category="BA|Gameplay")
	static bool EvaluateInput(float& InOutTimeDifference, float InCurrentTime, FMusicalNote* PreviousNote, FMusicalNote* NextNote);
	static void StripPlayedNotes(TArray<FMusicalNote, TInlineAllocator<256>>& NotesToCheck, float TimeToCompareWith);
	static void PlayUnlockSound();

	static void SetupConstraint(AActor* Actor, UPrimitiveComponent* Comp);

	static void FadeInMesh(UStaticMeshComponent*& MeshToFade, const float DeltaTime);
};
